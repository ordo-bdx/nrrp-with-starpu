#ifndef SUBROUTINE
#define SUBROUTINE

#include "rectangle.h"

void NRRP(double *surfaces,int P,zone **res) ;  //given the surfaces (unsorted and that will not be modified during the execution) and their number P, returns the repartition computed by NRRP using zone data structure (res).

void RRP(double *surfaces,int P,zone **res) ; //given the surfaces (unsorted and that will not be modified during the execution) and their number P, returns the repartition computed by RRP using zone data structure (res).

void SNRRP(double *surfaces,int P,zone **res) ; //given the surfaces (unsorted and that will not be modified during the execution) and their number P, returns the repartition computed by SNRRP using zone data structure (res).

#endif
