#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "rectangle.h"

double min(double a, double b){
  if(a <= b){
    return a ;
  }
  else {
    return b ;
  }
}

double max(double a, double b){
  if(a <= b){
    return b ;
  }
  else {
    return a ;
  }
}

void GuilloRec(rectangle rect,double alpha, rectangle res[2]){
  double h,w ;
  double x1,x2,y1,y2 ;
  x1 = rect.x1 ;
  x2 = rect.x2 ;
  y1 = rect.y1 ;
  y2 = rect.y2 ;
  h = x2-x1 ;
  w = y2-y1 ;
  rectangle R1,R2 ;
  if(h >= w){   
    R1.x1 = x1 ;
    R1.x2 = x1+alpha*h ; 
    R1.y1 = y1 ;
    R1.y2 = y2 ;
    R2.x1 = x1+alpha*h ;
    R2.x2 = x2;
    R2.y1 = y1 ;
    R2.y2 = y2 ;
      }
  else{    
    R1.x1 = x1 ;
    R1.x2 = x2 ;
    R1.y1 = y1 ;
    R1.y2 = y1 + alpha*w ;
    R2.x1 = x1 ;
    R2.x2 = x2 ;
    R2.y1 = y1+alpha*w ;
    R2.y2 = y2 ;
  }
  res[0] = R1 ;
  res[1] = R2 ;
}

void Guillo(rectangle rect,double alpha, zone ** res, int debut){
  rectangle resbis[2] ;
  GuilloRec(rect,alpha,resbis) ;
  zone *z1,*z2 ;
  z1 = malloc(sizeof(zone)) ;
  z2 = malloc(sizeof(zone)) ;
  z1 -> head = resbis[0] ;
  z1 -> queue = NULL ;
  z2 -> head = resbis[1] ;
  z2 -> queue = NULL ;
  res[debut] = z1 ;
  res[debut+1] = z2 ;
}  

void Square(rectangle rect, double alpha, zone **res,int debut){
  double h,w,s ;
  double x1,x2,y1,y2 ;
  x1 = rect.x1 ;
  x2 = rect.x2 ;
  y1 = rect.y1 ;
  y2 = rect.y2 ;
  h = x2-x1 ;
  w = y2-y1 ;
  s = h*w ;
  zone * z1,* z2,* z3 ;
  z1 = malloc(sizeof(zone)) ;
  z2 = malloc(sizeof(zone)) ;
  z3 = malloc(sizeof(zone)) ;
  rectangle R1,R2,R3 ;
  R1.x1 = x1 ;
  R1.x2 = x1 + sqrt(alpha*s) ;
  R1.y1 = y1 ;
  R1.y2 = y1 + sqrt(alpha*s) ;
  R2.x1 = x1 ;
  R2.x2 = R1.x2 ;
  R2.y1 = R1.y2 ;
  R2.y2 = y2 ;
  R3.x1 = R1.x2 ;
  R3.x2 = x2 ;
  R3.y1 = y1 ;
  R3.y2 = y2 ;
  z1->head = R1 ;
  z1->queue = NULL ;
  z3->head = R3 ;
  z3->queue = NULL ;
  z2->head = R2 ;
  z2->queue = z3 ;
  res[debut] = z1 ;
  res[debut+1] = z2 ;
}

void Tripartition(rectangle rect, double alpha, double beta, zone **res,int debut){
  double h,w ;
  double x1,x2,y1,y2 ;
  x1 = rect.x1 ;
  x2 = rect.x2 ;
  y1 = rect.y1 ;
  y2 = rect.y2 ;
  h = x2-x1 ;
  w = y2-y1 ;
  rectangle R1,R2,R3 ;
  if(h>=w){
    double h1 = (alpha+beta)*h ;
    double w1 = (alpha/(alpha+beta))*w ;
    R1.x1 = x1 ;
    R1.x2 = x1 +h1 ;
    R1.y1 = y1 ;
    R1.y2 = y1+w1 ;
    R2.x1 = x1 ;
    R2.x2 = x1 + h1 ;
    R2.y1 = y1+w1 ;
    R2.y2 = y2 ;
    R3.x1 = x1+h1 ;
    R3.x2 = x2 ;
    R3.y1 = y1 ;
    R3.y2 = y2 ;
  }
  else {
    double w1 = (alpha+beta)*w ;
    double h1 = (alpha/(alpha+beta))*h ;
    R1.x1 = x1 ;
    R1.x2 = x1 +h1 ;
    R1.y1 = y1 ;
    R1.y2 = y1+w1 ;
    R2.x1 = x1+h1 ;
    R2.x2 = x2 ;
    R2.y1 = y1 ;
    R2.y2 = y1+w1 ;
    R3.x1 = x1 ;
    R3.x2 = x2 ;
    R3.y1 = y1+w1 ;
    R3.y2 = y2 ;
  }
  zone *z1,*z2,*z3 ;
  z1 = malloc(sizeof(zone)) ;
  z2 = malloc(sizeof(zone)) ;
  z3 = malloc(sizeof(zone)) ;
  z1 -> head = R1 ;
  z1 -> queue = NULL ;
  z2 -> head = R2 ;
  z2 -> queue = NULL ;
  z3 -> head = R3 ;
  z3 -> queue = NULL ;
  res[debut] = z1 ;
  res[debut+1] = z2 ;
  res[debut+2] = z3 ;
}

void Superposition(rectangle rect,double alpha, double epsilon, zone **res, int debut){
  double h,w,s,l ;
  double x1,x2,y1,y2 ;
  x1 = rect.x1 ;
  x2 = rect.x2 ;
  y1 = rect.y1 ;
  y2 = rect.y2 ;
  h = x2-x1 ;
  w = y2-y1 ;
  s= h*w ;
  l = sqrt(epsilon*s) ;
  rectangle R1,R2,R3,R4 ;
  R1.x1 = x1 ;
  R1.x2 = x1 + l ;
  R1.y1 = y1 ;
  R1.y2 = y1 + l ;
  if(h>= w){
    double h1 = (alpha-epsilon)*s/(w-l) ;
    R2.x1 = x1 ;
    R2.x2 = x1 + h1 ;
    R2.y1 = y1+l ;
    R2.y2 = y2 ;
    R3.x1 = x1+l ;
    R3.x2 = x1+h1 ;
    R3.y1 = y1 ;
    R3.y2 = y1+l ;
    R4.x1 = x1+h1 ;
    R4.x2 = x2 ;
    R4.y1 = y1 ;
    R4.y2 = y2 ;
  }
  else {
    double w1 = (alpha-epsilon)*s/(h-l) ;
    R2.x1 = x1+l ;
    R2.x2 = x2 ;
    R2.y1 = y1 ;
    R2.y2 = y1+w1 ;
    R3.x1 = x1 ;
    R3.x2 = x1+l ;
    R3.y1 = y1+l ;
    R3.y2 = y1+w1 ;
    R4.x1 = x1 ;
    R4.x2 = x2 ;
    R4.y1 = y1+w1 ;
    R4.y2 = y2 ;
  }
  zone *z1,*z2,*z3,*z4 ;
  z1 = malloc(sizeof(zone)) ;
  z2 = malloc(sizeof(zone)) ;
  z3 = malloc(sizeof(zone)) ;
  z4 = malloc(sizeof(zone)) ;
  z1 -> head = R1 ;
  z1 -> queue = NULL ;
  z2 -> head = R2 ;
  z2 -> queue = NULL ;
  z4 -> head = R4 ;
  z4 -> queue = NULL ;
  z3 -> head = R3 ;
  z3 -> queue = z4 ;
  res[debut] = z1 ;
  res[debut+1] = z2 ;
  res[debut+2] = z3 ;
}
  
void Packing(rectangle rect,double *surfaces,int n, int dsur, rectangle * res,int *debut, int * fin, int * nb_zones){
  double h,w,s ;
  double x1,x2,y1,y2 ;
  x1 = rect.x1 ;
  x2 = rect.x2 ;
  y1 = rect.y1 ;
  y2 = rect.y2 ;
  h = x2-x1 ;
  w = y2-y1 ;
  s= h*w ;
  double rho = max(h,w)/min(h,w) ;
  double alpha = 1/rho ;
  if(surfaces[dsur+n-1]+surfaces[dsur+n-2] > 5*alpha*s/2){
    double sprime = s- surfaces[dsur+n-1] ;
    if (surfaces[dsur+n-2] >= 2*alpha*s/5){
      rectangle recs[2] ;
      GuilloRec(rect,sprime/s,recs) ;
      rectangle R3 = recs[1] ;
      GuilloRec(recs[0],(sprime-surfaces[dsur+n-2])/sprime,recs) ;
      *nb_zones = 3 ;
      res[0] = recs[0] ;
      res[1] = recs[1] ;
      res[2]  = R3 ;
      debut[0] = 0 ;
      fin[0] = n-3 ;
      debut[1] = n-2 ;
      fin[1] = n-2 ;
      debut[2] = n-1 ;
      fin[2] = n-1 ;
    }
    else{
      int i = -1 ;
      double sur = 0 ;
      while(sur <= sprime - 2*alpha*s/5){
	i = i+1 ;
	sur = sur + surfaces[dsur+i] ;
      } ;
      sur = sur - surfaces[dsur+i] ;
      i = i -1 ;
      rectangle recs[2] ;
      GuilloRec(rect,sprime/s,recs) ;
      rectangle R3 = recs[1] ;
      GuilloRec(recs[0],sur/sprime,recs) ;
      *nb_zones = 3 ;
      res[0] = recs[0] ;
      res[1] = recs[1] ;
      res[2] = R3 ;
      debut[0] = 0 ;
      fin[0] = i ;
      debut[1] = i+1 ;
      fin[1] = n-2 ;
      debut[2] = n-1 ;
      fin[2] = n-1 ;
    }
  }
  else{
    int i = n-1 ;
    int *debut_aux,*fin_aux ;
    debut_aux = malloc(n*sizeof(int)) ;
    fin_aux = malloc(n*sizeof(int)) ;
    int j = 0 ;
    while(i >= 0){
      double sur = 0 ;
      int k = i ;
      while(i >= 0 && sur < 2*alpha*s/5){
	sur = sur + surfaces[dsur+i] ;
	i = i-1 ;
      } ;
      if(sur >= 2*alpha*s/5){
	debut_aux[j] = i+1;
	fin_aux[j] = k;
	j = j+1 ;
      }
      else{
	debut_aux[j-1] = 0;
      }       
    } ;
    double sprime = s ;
    int m = j ;
    j = j-1 ;
    rectangle aux_rec[2] ;
    aux_rec[0] = rect ;
    *nb_zones = m ;
    while(j >= 0){
      debut[j] = debut_aux[m-j-1] ;
      fin[j] = fin_aux[m-j-1] ;
      double sum = 0;
      int k ;
      for(k=debut[j];k <= fin[j];k++){
	sum = sum + surfaces[dsur+k] ;
      } ;
      GuilloRec(aux_rec[0],(sprime-sum)/sprime,aux_rec) ;
      res[j] = aux_rec[1] ;
      sprime = sprime-sum ;
      j= j-1 ;
    } ;
    free(debut_aux) ;
    free(fin_aux) ;
  }
}
