#ifndef SUBROUTINE
#define SUBROUTINE

#include "rectangle.h"

void GuilloRec(rectangle rect,double alpha, rectangle res[2]) ;

void Guillo(rectangle rect,double alpha, zone **res, int debut) ;

void Square(rectangle rect,double alpha, zone **res, int debut) ;

void Tripartition(rectangle rect, double alpha, double beta, zone **res, int debut) ;

void Superposition(rectangle rect,double alpha, double epsilon, zone **res, int debut) ;

void Packing(rectangle rect,double *surfaces,int n, int dsur, rectangle * res,int *debut, int * fin, int * nb_zones) ;

#endif
