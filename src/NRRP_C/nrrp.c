#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "rectangle.h"
#include "subroutine.h"

void RecPartNRRP(rectangle R,double *surfaces,int debut, int fin,zone **res){
  if(debut == fin){
    zone *z ;
    z = malloc(sizeof(zone)) ;
    z -> head = R ;
    z -> queue = NULL ;
    res[debut] = z ;
  }
  else {
    double h,w,s ;
    double x1,x2,y1,y2 ;
    x1 = R.x1 ;
    x2 = R.x2 ;
    y1 = R.y1 ;
    y2 = R.y2 ;
    h = x2-x1 ;
    w = y2-y1 ;
    s= h*w ;
    double rho = max(h,w)/min(h,w) ;
    int k = 0;
    double s1 = 0.;
    while(s1 < 2*s/(5*rho)){
      s1 = s1 + surfaces[debut+k] ; 
      k++ ;
    } ;
    if(k+debut <= fin){ 
      if(s-s1 >= 2*s/(5*rho)){
	rectangle recs[2] ;
	GuilloRec(R,s1/s,recs) ;
	RecPartNRRP(recs[0],surfaces,debut,debut+k-1,res) ;
	RecPartNRRP(recs[1],surfaces,debut+k,fin,res) ;	
      }
      else {
	Tripartition(R,(s1-surfaces[fin-1])/s,surfaces[fin-1]/s,res,fin-2) ;
	RecPartNRRP(res[fin-2]->head,surfaces,debut,fin-2,res) ;
      }
    }
    else{
      s1 = s1 - surfaces[fin] ;
      if(s1/s<= 1 - 3*(rho+1)*(rho+1)/(16*rho)){
	Square(R,s1/s,res,fin-1) ;
	RecPartNRRP(res[fin-1]->head,surfaces,debut,fin-1,res) ;
      }
      else{
	if(fin-debut == 1){
	  Guillo(R,s1/s,res,debut) ;
	}
	else{
	  double s2 = s1 - surfaces[fin-1] ;
	  if(s2 >= 2*rho*s1*s1/(5*s)){
	    if(s2 <= 5*rho*s1*s1/(2*s)){
	      rectangle recs[2];
	      zone *z1,*z2 ;
	      z1 = malloc(sizeof(zone)) ;
	      z2 = malloc(sizeof(zone)) ;
	      GuilloRec(R,s1/s,recs) ;
	      z1 -> head = recs[1] ;
	      z1 -> queue = NULL ;
	      res[fin] = z1 ;
	      GuilloRec(recs[0],s2/s1,recs) ;
	      z2 -> head = recs[1] ;
	      z2 -> queue = NULL ;
	      res[fin-1] = z2 ;
	      RecPartNRRP(recs[0],surfaces,debut,fin-2,res) ;
	    }
	    else{
	      double s3 = s2 - surfaces[fin-2] ;
	      if(s3 >= 2*rho*s1*s1/(5*s)){
		rectangle recs[2] ;
		zone *z ;
		z = malloc(sizeof(zone)) ;
		GuilloRec(R,s1/s,recs) ;
		z -> head = recs[1] ;
		z -> queue = NULL ;
		res[fin] = z ;
		rectangle *rects ;
		int *debutP, *finP, *nb_zones ;
		rects = malloc((fin-debut)*sizeof(rectangle)) ;
		debutP = malloc((fin-debut)*sizeof(int)) ;
		finP = malloc((fin-debut)*sizeof(int)) ;
		nb_zones = malloc(sizeof(int)) ;
		Packing(recs[0],surfaces,fin-debut,debut,rects,debutP,finP,nb_zones) ;
		int i ;
		for(i=0;i<*nb_zones;i++){
		  RecPartNRRP(rects[i],surfaces,debutP[i]+debut,finP[i]+debut,res) ;
		} ;	    
		free(debutP) ;
		free(finP) ;
		free(rects) ;
		free(nb_zones) ;
	      }
	      else{
		if(s3/s <= (1-sqrt(1-rho*s1/s))*(1-sqrt(1-rho*s1/s))/rho){
		  rectangle recs[2] ;
		  zone *z1,*z2 ;
		  z1 = malloc(sizeof(zone)) ;
		  z2 = malloc(sizeof(zone)) ;
		  GuilloRec(R,s1/s,recs) ;
		  z1 -> head = recs[1] ;
		  z1 -> queue = NULL ;
		  res[fin] = z1 ;
		  GuilloRec(recs[0],s2/s1,recs) ;
		  z2 -> head = recs[1] ;
		  z2 -> queue = NULL ;
		  res[fin-1] = z2 ;
		  Square(recs[0],s3/s2,res,fin-3) ;
		  RecPartNRRP(res[fin-3]->head,surfaces,debut,fin-3,res) ;
		}
		else{
		  Superposition(R,s1/s,s3/s,res,fin-2) ;
		  RecPartNRRP(res[fin-2]->head,surfaces,debut,fin-3,res) ;
		  Guillo(res[fin-1]->head,(s2-s3)/(s1-s3),res,fin-2) ;
		}
	      }
	    }
	  }
	  else{
	    if(s2/s <= (1-sqrt(1-rho*s1/s))*(1-sqrt(1-rho*s1/s))/rho){
	      Guillo(R,s1/s,res,fin-1) ;
	      Square(res[fin-1]->head,s2/s1,res,fin-2) ;
	      RecPartNRRP(res[fin-2]->head,surfaces,debut,fin-2,res) ;
	    } 
	    else{
	      Superposition(R,s1/s,s2/s,res,fin-2) ;
	      RecPartNRRP(res[fin-2]->head,surfaces,debut,fin-2,res) ;
	    }
	  }
	}
      }
    }
  }
}

void NRRP(double *surfaces,int n,zone **res){
  rectangle R ;
  R.x1 = 0. ;
  R.x2 = 1. ;
  R.y1 = 0. ;
  R.y2 = 1. ;
  int cmp_func(const void *a,const void *b){
    double x, y ;
    x = surfaces[*(int *) a];
    y = surfaces[*(int *) b];
    if(fabs(x-y) < 1e-10) return 0; 
    if (x < y) return -1; 
    return 1; 
  } ;
  int *ind ;
  double *new_surfaces ;
  ind = malloc(n*sizeof(int)) ;
  int i ;
  for(i=0;i<n;i++){
    ind[i] = i ;
  }
  zone **res_bis ;
  res_bis=malloc(n*sizeof(zone*)) ;
  new_surfaces = malloc(n*sizeof(double)) ;
  qsort(ind,n,sizeof(int),cmp_func) ;
  for(i=0;i<n;i++){
    new_surfaces[i] = surfaces[ind[i]] ;
  }
  RecPartNRRP(R,new_surfaces,0,n-1,res_bis) ;
  for(i=0;i<n;i++){
    res[ind[i]] = res_bis[i]  ;
  }
  free(ind) ;
  free(res_bis) ;
  free(new_surfaces) ;
}




void RecPartRRP(rectangle R,double *surfaces,int debut, int fin,zone **res){
  if(debut == fin){
    zone *z ;
    z = malloc(sizeof(zone)) ;
    z -> head = R ;
    z -> queue = NULL ;
    res[debut] = z ;
  }
  else {
    double h,w,s ;
    double x1,x2,y1,y2 ;
    x1 = R.x1 ;
    x2 = R.x2 ;
    y1 = R.y1 ;
    y2 = R.y2 ;
    h = x2-x1 ;
    w = y2-y1 ;
    s= h*w ;
    int k = 0;
    double s1 = 0.;
    while(s1 < s/3){
      s1 = s1 + surfaces[debut+k] ; 
      k++ ;
    } ;
    rectangle recs[2] ;
    if(k+debut <= fin){ 
      GuilloRec(R,s1/s,recs) ;
      RecPartRRP(recs[0],surfaces,debut,debut+k-1,res) ;
      RecPartRRP(recs[1],surfaces,debut+k,fin,res) ;    
    }
    else{
      s1 = s - surfaces[fin] ;
      GuilloRec(R,s1/s,recs) ;
      RecPartRRP(recs[0],surfaces,debut,fin-1,res) ;
      RecPartRRP(recs[1],surfaces,fin,fin,res) ;
    }
  }
}

void RRP(double *surfaces,int n,zone **res){
  rectangle R ;
  R.x1 = 0. ;
  R.x2 = 1. ;
  R.y1 = 0. ;
  R.y2 = 1. ;
  int cmp_func(const void *a,const void *b){
    double x, y ;
    x = surfaces[*(int *) a];
    y = surfaces[*(int *) b];
    if(fabs(x-y) < 1e-10) return 0; 
    if (x < y) return -1; 
    return 1; 
  } ;
  int *ind ;
  double *new_surfaces ;
  ind = malloc(n*sizeof(int)) ;
  int i ;
  for(i=0;i<n;i++){
    ind[i] = i ;
  }
  zone **res_bis ;
  res_bis=malloc(n*sizeof(zone)) ;
  new_surfaces = malloc(n*sizeof(double)) ;
  qsort(ind,n,sizeof(int),cmp_func) ;
  for(i=0;i<n;i++){
    new_surfaces[i] = surfaces[ind[i]] ;
  }
  RecPartRRP(R,new_surfaces,0,n-1,res_bis) ;
  for(i=0;i<n;i++){
    res[ind[i]] = res_bis[i]  ;
  }
  free(ind) ;
  free(res_bis) ;
  free(new_surfaces) ;
}


void RecPartSNRRP(rectangle R,double *surfaces,int debut, int fin,zone **res){
  if(debut == fin){
    zone *z ;
    z = malloc(sizeof(zone)) ;
    z -> head = R ;
    z -> queue = NULL ;
    res[debut] = z ;
  }
  else {
    double h,w,s ;
    double x1,x2,y1,y2 ;
    x1 = R.x1 ;
    x2 = R.x2 ;
    y1 = R.y1 ;
    y2 = R.y2 ;
    h = x2-x1 ;
    w = y2-y1 ;
    s= h*w ;
    double rho = max(h,w)/min(h,w) ;
    int k = 0;
    double s1 = 0.;
    while(s1 < s/(3*rho)){
      s1 = s1 + surfaces[debut+k] ; 
      k++ ;
    } ;
    if(k+debut <= fin){ 
      rectangle recs[2] ;
      GuilloRec(R,s1/s,recs) ;
      RecPartRRP(recs[0],surfaces,debut,debut+k-1,res) ;
      RecPartRRP(recs[1],surfaces,debut+k,fin,res) ;    
    }
    else{
      s1 = s - surfaces[fin] ;
      Square(R,s1/s,res,fin-1) ;
      RecPartNRRP(res[fin-1]->head,surfaces,debut,fin-1,res) ;
    }
  }
}


void SNRRP(double *surfaces,int n,zone **res){
  rectangle R ;
  R.x1 = 0. ;
  R.x2 = 1. ;
  R.y1 = 0. ;
  R.y2 = 1. ;
  int cmp_func(const void *a,const void *b){
    double x, y ;
    x = surfaces[*(int *) a];
    y = surfaces[*(int *) b];
    if(fabs(x-y) < 1e-10) return 0; 
    if (x < y) return -1; 
    return 1; 
  } ;
  int *ind ;
  double *new_surfaces ;
  ind = malloc(n*sizeof(int)) ;
  int i ;
  for(i=0;i<n;i++){
    ind[i] = i ;
  }
  zone **res_bis ;
  res_bis=malloc(n*sizeof(zone)) ;
  new_surfaces = malloc(n*sizeof(double)) ;
  qsort(ind,n,sizeof(int),cmp_func) ;
  for(i=0;i<n;i++){
    new_surfaces[i] = surfaces[ind[i]] ;
  }
  RecPartSNRRP(R,new_surfaces,0,n-1,res_bis) ;
  for(i=0;i<n;i++){
    res[ind[i]] = res_bis[i]  ;
  }
  free(ind) ;
  free(res_bis) ;
  free(new_surfaces) ;
}
