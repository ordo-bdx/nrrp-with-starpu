#ifndef RECTANGLE
#define RECTANGLE

typedef struct{
  	double x1 ;
  	double x2 ;
  	double y1 ;
  	double y2 ;
} rectangle;

typedef struct node{
	rectangle head ;
	struct node *queue ;	
} zone ;

typedef struct{
	rectangle covering ;
	zone * complement ;
} rzone ;

typedef struct{
  	int x1 ;
  	int x2 ;
  	int y1 ;
  	int y2 ;
} drectangle;

typedef struct dnode{
	drectangle head ;
	struct dnode *queue ;	
} dzone ;

void freeZone(zone *z) ;
void freeDZone(dzone *z) ;
#endif
