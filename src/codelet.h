#include <starpu.h>
#include <starpu_scheduler.h>
#include <math.h>
#include "NRRP_C/nrrp.h"
#include "scheduling.h"
#ifndef CODELETGEMM
#define CODELETGEMM

int l,N ;

void cpu_gemm_func(void *buffers[], void *cl_arg) ;

void gpu_gemm_func(void *buffers[], void *cl_arg) ;

void warm_up_gpu(void *descr[], void *cl_arg) ;

void warm_up_cpu(void *descr[], void *cl_arg) ;

void init_cpu_func(void *descr[], void *cl_arg) ;

void init_gpu_func(void *descr[], void *cl_arg) ;

void redux_cpu_func(void *descr[], void *cl_arg) ;

void redux_gpu_func(void *descr[], void *cl_arg) ;


struct starpu_perfmodel gemm_perf_model; 

struct starpu_codelet cl; 

struct starpu_perfmodel gemm_perf_model_redux; 

struct starpu_codelet cl_redux; 

struct starpu_codelet cl_warm_up; 

struct starpu_codelet init_codelet; 

struct starpu_codelet redux_codelet; 

#endif
