#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "../NRRP_C/rectangle.h"
#include "../NRRP3D_C/cuboid.h"


#define GET2(A,i,j,N) A[(i)+(N)*(j)]
#define GET3(A,i,j,k,N) A[(i)+(N)*(j)+(N)*(N)*(k)]

int compare_function(const void *a,const void *b) {
  double *x = (double *) a;
  double *y = (double *) b;
  if (fabs(*x - *y) < 1e-10) return 0;
  else if (*x > *y) return 1; return -1;
}


double partial_sum(double *surfaces,int i ,int j){
  double res = 0 ;
  int k ;
  for(k=i;k<=j;k++){
    res = res + surfaces[k] ;
  }
  return res ;
}

void column_based(double *surfaces,int p,int *nb_procs_by_column,int *nb_columns,double *com){
  qsort(surfaces,p,sizeof(double),compare_function) ;
  double *partial ;
  partial = malloc(p*p*sizeof(double)) ;
  int dep,fin ;
  for(dep = 0;dep<p;dep++){
    for(fin = dep;fin<p;fin++){
      if(fin == dep){
	GET2(partial,dep,fin,p) = surfaces[dep] ;	
      }
      else{
	GET2(partial,dep,fin,p) = GET2(partial,dep,fin-1,p) + surfaces[fin] ;
      }
    }
  }
  int q ;
  double * f ;
  int * f_cut ;
  f = malloc((p+1)*(p+1)*sizeof(double)) ;
  f_cut = malloc((p+1)*(p+1)*sizeof(int)) ;
  for(q=1;q<=p;q++){
    GET2(f,1,q,p+1) = GET2(partial,0,p-1,p) + GET2(partial,0,q-1,p) * q ;
    GET2(f_cut,1,q,p+1) = 0 ;
  }
  int C ;
  double aux ;
  for(C = 2;C<=p;C++){
    for(q = C;q<=p;q++){
      int r,r_min ;
      r = 1 ;
      r_min = 1 ;
      GET2(f,C,q,p+1) = GET2(partial,0,p-1,p) + GET2(f,C-1,q-1,p+1) + surfaces[q-1] ;
      for(r=2;r<=q-C+1;r++){
	aux = GET2(partial,0,p-1,p) + GET2(f,C-1,q-r,p+1) + r*GET2(partial,q-r,q-1,p) ;
        if(aux < GET2(f,C,q,p+1)){
	  GET2(f,C,q,p+1) = aux ;
	  r_min = r ;
	}
      }
      GET2(f_cut,C,q,p+1) = q - r_min ;
    }
  }
  int C_min =1 ;
  for(C=2;C<=p;C++){
    if(GET2(f,C,p,p+1) < GET2(f,C_min,p,p+1)){
      C_min = C ;
    }
  }
  q = p ;
  *nb_columns = C_min ;
  *com = GET2(f,C_min,p,p+1) ;
  for(C = C_min;C >= 2;C--){
    nb_procs_by_column[C-1] = q - GET2(f_cut,C,q,p+1) ;
     q =  GET2(f_cut,C,q,p+1) ;
  }
  nb_procs_by_column[0] = q ;
  free(f) ;
  free(f_cut) ;
  free(partial) ;
}

void ColumnBasedRepart(double *surfaces,int P, zone **res){
    int cmp_func(const void *a,const void *b){
    double x, y ;
    x = surfaces[*(int *) a];
    y = surfaces[*(int *) b];
    if(fabs(x-y) < 1e-10) return 0; 
    if (x < y) return -1; 
    return 1; 
  } ;
  int *ind ;
  double *new_surfaces ;
  ind = malloc(P*sizeof(int)) ;
  int i ;
  for(i=0;i<P;i++){
    ind[i] = i ;
  }
  zone **res_bis ;
  res_bis=malloc(P*sizeof(zone*)) ;
  new_surfaces = malloc(P*sizeof(double)) ;
  qsort(ind,P,sizeof(int),cmp_func) ;
  for(i=0;i<P;i++){
    new_surfaces[i] = surfaces[ind[i]] ;
  }
  int nb_columns ;
  double com ;
  int *nb_proc_by_column = malloc(P*sizeof(int)) ;
  column_based(new_surfaces,P,nb_proc_by_column,&nb_columns,&com) ;
  int j ;
  double s1,s2 ;
  s1 = 0 ;
  int k = 0 ;
  double h=1 ;
  for(i=0;i<nb_columns;i++){
    s2 = partial_sum(new_surfaces,k,k+nb_proc_by_column[i]-1) ;
    h = 0 ;
    for(j=0;j<nb_proc_by_column[i];j++){
      zone *z = malloc(sizeof(zone)) ;
      rectangle R ;
      R.x1 = s1 ; R.x2 = s1+s2 ;R.y1 = h ; R.y2 = h+new_surfaces[k+j]/s2 ;
      z-> head = R ;
      z -> queue = NULL ;
      res_bis[k+j] = z ;
      h = R.y2 ;
    }        
    k = k + nb_proc_by_column[i] ;
    s1 = s1+s2 ;
  }
  for(i=0;i<P;i++){
    res[ind[i]] = res_bis[i]  ;
  }
  free(ind) ;
  free(res_bis) ;
  free(new_surfaces) ;
  free(nb_proc_by_column) ;
}

void normalize(double *volumes,double *surfaces,int i ,int j){
  double sum = partial_sum(volumes,i,j) ;
  int k ;
  for(k=i;k<=j;k++){
    surfaces[k-i]=volumes[k]/sum ;
  }
}

void SCR(double *volumes,int p,int *nb_procs_by_slice,int *nb_slice, double *com){
  double * f = malloc((p+1)*(p+1)*sizeof(double));
  int * f_cut = malloc((p+1)*(p+1)*sizeof(int)) ;
  int q,C ;
  double *surfaces = malloc(p*sizeof(double)) ; 
  double *com_2D = malloc(sizeof(double)) ; 
  int *temp1 = malloc(p*sizeof(int)) ; 
  int *temp2 = malloc(sizeof(int)) ;
  double *partial ;
  partial = malloc(p*p*sizeof(double)) ;
  int dep,fin ;
  for(dep = 0;dep<p;dep++){
    for(fin = dep;fin<p;fin++){
      if(fin == dep){
	GET2(partial,dep,fin,p) = volumes[dep] ;	
      }
      else{
	GET2(partial,dep,fin,p) = GET2(partial,dep,fin-1,p) + volumes[fin] ;
      }
    }
  }

  // pré-calcul Colum Based

  double *cb = malloc((p+1)*(p+1)*(p+1)*sizeof(double)) ;
  if (cb == NULL){
    printf("erreur malloc\n") ;
    exit(EXIT_FAILURE) ;
  }
  for(dep=0;dep<p;dep++){
    for(fin=dep;fin<p;fin++){
      GET3(cb,1,dep,fin,p+1) = (fin-dep+2)*GET2(partial,dep,fin,p) ;
    }
  }
  for(dep=0;dep<p;dep++){
    for(C=2;C<=p;C++){
      for(fin=dep+C-1;fin<p;fin++){
	GET3(cb,C,dep,fin,p+1) = GET2(partial,dep,fin,p) + GET3(cb,C-1,dep,fin-1,p+1) + (C-1)*volumes[fin] + volumes[fin] ;
	int r ;
	for(r=2;r<=(fin-dep+1)-C+1;r++){
	  double aux = GET2(partial,dep,fin,p) + GET3(cb,C-1,dep,fin-r,p+1) + (C-1) * GET2(partial,fin-r+1,fin,p) + r*GET2(partial,fin-r+1,fin,p) ;
	  if (aux < GET3(cb,C,dep,fin,p+1)){
	    GET3(cb,C,dep,fin,p+1) = aux ;
	  }
	}
      }
    }
  }
  double *cb_opt = malloc(p*p*sizeof(double)) ;
  int *nb_col_opt = malloc(p*p*sizeof(int)) ;
  for(dep=0;dep<p;dep++){
    for(fin=dep;fin<p;fin++){
      int C_min = 1 ;
      for(C=2;C<=fin-dep+1;C++){
	if(GET3(cb,C,dep,fin,p+1)<GET3(cb,C_min,dep,fin,p+1)){
	  C_min = C ;
	}
      }
      GET2(cb_opt,dep,fin,p) = GET3(cb,C_min,dep,fin,p+1) ;
      GET2(nb_col_opt,dep,fin,p) = C_min ;
    }
  }
  free(cb) ;

  for(q=1;q<=p;q++){
    GET2(f,1,q,p+1) = 1 + GET2(cb_opt,0,q-1,p) ;
    GET2(f_cut,1,q,p+1) = 0 ;
  }
  int S ;
  double aux ;
  for(S=2;S<=p;S++){
    for(q=S;q<=p;q++){
      int r,r_min ;
      r = 1 ;
      r_min = 1 ;
      GET2(f,S,q,p+1) = 1 + GET2(f,S-1,q-1,p+1) + 2*volumes[q-1] ;
      for(r=2;r<=q-S+1;r++){
	aux = 1 + GET2(f,S-1,q-r,p+1) + GET2(cb_opt,q-r,q-1,p) ;
	if(aux < GET2(f,S,q,p+1)){
	  GET2(f,S,q,p+1) = aux ;
	  r_min = r ;
	}
      }
      GET2(f_cut,S,q,p+1) = q - r_min ;      
    }
  }
  int S_min =1 ;
  for(S=2;S<=p;S++){
    if(GET2(f,S,p,p+1) < GET2(f,S_min,p,p+1)){
      S_min = S ;
    }
  }
  q = p ;
  *nb_slice = S_min ;
  *com = GET2(f,S_min,p,p+1) ;
  for(S = S_min;S >= 2;S--){
    nb_procs_by_slice[S-1] = q - GET2(f_cut,S,q,p+1) ;
     q =  GET2(f_cut,S,q,p+1) ;
  }
  nb_procs_by_slice[0] = q ;
  free(f) ;
  free(f_cut) ;
  free(partial) ;
  free(cb_opt) ;
  free(nb_col_opt) ;
  free(surfaces) ;
  free(temp1) ;
  free(temp2) ;
  free(com_2D) ;
}


void SCRRepart(double *volumes,int P, zone3D **res){
  int cmp_func(const void *a,const void *b){
    double x, y ;
    x = volumes[*(int *) a];
    y = volumes[*(int *) b];
    if(fabs(x-y) < 1e-10) return 0; 
    if (x < y) return -1; 
    return 1; 
  } ;
  int *ind ;
  double *new_volumes ;
  ind = malloc(P*sizeof(int)) ;
  int i ;
  for(i=0;i<P;i++){
    ind[i] = i ;
  }
  zone3D **res_bis ;
  res_bis=malloc(P*sizeof(zone3D*)) ;
  new_volumes = malloc(P*sizeof(double)) ;
  qsort(ind,P,sizeof(int),cmp_func) ;
  for(i=0;i<P;i++){
    new_volumes[i] = volumes[ind[i]] ;
  }
  int nb_slices ;
  double com ;
  int *nb_proc_by_slice = malloc(P*sizeof(int)) ;
  SCR(new_volumes,P,nb_proc_by_slice,&nb_slices,&com) ;
  int j ;
  double s1,s2 ;
  s1 = 0 ;
  int k = 0 ;
  for(i=0;i<nb_slices;i++){
    s2 = partial_sum(new_volumes,k,k+nb_proc_by_slice[i]-1) ;
    double *volume_aux = malloc(nb_proc_by_slice[i]*sizeof(double)) ;
    normalize(new_volumes,volume_aux,k,k+nb_proc_by_slice[i]-1) ;
    zone **res_aux = malloc(nb_proc_by_slice[i]*sizeof(zone*)) ;
    ColumnBasedRepart(volume_aux,nb_proc_by_slice[i],res_aux) ;
    for(j=0;j<nb_proc_by_slice[i];j++){
      zone3D *z = malloc(sizeof(zone3D)) ;
      cuboid C  ;
      rectangle R = res_aux[j]->head ;
      C.x1 = R.x1 ; C.x2 = R.x2 ; C.y1 = R.y1 ; C.y2 = R.y2 ; C.z1 = s1 ; C.z2 = s1+s2 ;
      z-> head = C ;
      z -> queue = NULL ;
      res_bis[k+j] = z ;
      freeZone(res_aux[j]) ;
    }        
    k = k + nb_proc_by_slice[i] ;
    s1 = s1+s2 ;
    free(volume_aux) ;
    free(res_aux) ;
  }
  for(i=0;i<P;i++){
    res[ind[i]] = res_bis[i]  ;
  }
  free(ind) ;
  free(res_bis) ;
  free(new_volumes) ;
  free(nb_proc_by_slice) ;
}
