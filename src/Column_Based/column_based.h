#ifndef CB
#define CB

#include "../NRRP_C/rectangle.h"
#include "../NRRP3D_C/cuboid.h"

void column_based(double *surfaces,int p,int *nb_procs_by_column,int *nb_columns,double *com) ; //given the surfaces (that will be sorted during the execution), their number p, colum_based returns the number of columns in the partitioning (nb_columns), the number of processors by columns (nb_procs_by_column) and the resulting amount of communication (com).

void ColumnBasedRepart(double *surfaces,int P, zone **res) ; //given the surfaces (unsorted and that will not be modified during the execution) and their number P, returns the repartition using zone data structure (res).

void SCR(double *volumes,int p,int *nb_procs_by_slice,int *nb_slice, double *com) ;//given the volumes (that will be sorted), their number p, returns the number of slices in the partitioning (nb_slice), the number of processors by slices (nb_procs_by_slice) and the resulting amount of communication.

void SCRRepart(double *volumes,int P, zone3D **res); //given the volumes (unsorted and that will not be modified) and their number P, returns the repartition using zone3D data structure (res).

#endif
