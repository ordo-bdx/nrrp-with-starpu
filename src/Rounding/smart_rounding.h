#ifndef SMART_ROUNDING
#define SMART_ROUNDING

#include "../NRRP_C/rectangle.h"
#include "../NRRP3D_C/cuboid.h"

void SmartRounding(double *surfaces, int P, zone **res,int N,int *rep) ;

void SmartRounding3D(double *volumes, int P, zone3D **res,int N,int *rep) ;


#endif
