#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../NRRP_C/rectangle.h"
#include "../NRRP_C/nrrp.h"
#include "../NRRP3D_C/nrrp.h"
#include "../Column_Based/column_based.h"
#include "../Hilbert_C/hilbert.h"
#include "rounding.h"
#include "smart_rounding.h"


#define GET2(A,i,j,N) A[(i)+(N)*(j)]
#define GET3(A,i,j,k,N) A[(i)+(N)*(j)+(N)*(N)*k]

#define ALG_CB 0
#define ALG_RRP 1
#define ALG_SNRRP 2
#define ALG_NRRP 3
#define ALG_HILBERT2D 4
#define ALG_SCR 5
#define ALG_NRRP3D 6
#define ALG_HILBERT3D 7

#define BRUTAL_ROUNDING 0
#define CLEVER_ROUNDING 1


void Rep2Dto3D(int *rep2D, int *rep3D, int N){
  int i,j,k ;
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      for(k=0;k<N;k++){
	GET3(rep3D,i,j,k,N) = GET2(rep2D,i,j,N) ;
      }
    }
  }
}

void RepRectangle(drectangle R,int *rep,int N, int proc){
  int x1 = R.x1 ; int x2 = R.x2 ; int y1 = R.y1 ; int y2 = R.y2 ;
  int i,j ;
  for(i=x1;i<x2;i++){
    for(j=y1;j<y2;j++){
      GET2(rep,i,j,N) = proc ;
    }
  }
}

void RepZone(dzone *z,int *rep,int N,int proc){
  if(z != NULL){
    RepRectangle(z->head,rep,N,proc) ;
    RepZone(z->queue,rep,N,proc) ;
  }
}

void Rep2D(dzone **res,int P,int *rep, int N){
  int i ;
  for(i=0;i<P;i++){
    RepZone(res[i],rep,N,i) ;
    freeDZone(res[i]) ;
  }
}


void RepCuboid(dcuboid C,int *rep,int N, int proc){
  int x1 = C.x1 ; int x2 = C.x2 ; int y1 = C.y1 ; int y2 = C.y2 ; int z1 = C.z1 ; int z2 = C.z2 ;
  int i,j,k ;
  for(i=x1;i<x2;i++){
    for(j=y1;j<y2;j++){
      for(k=z1;k<z2;k++){
	GET3(rep,i,j,k,N) = proc ;
      }
    }
  }
}

void RepZone3D(dzone3D *z,int *rep,int N,int proc){
  if(z != NULL){
    RepCuboid(z->head,rep,N,proc) ;
    RepZone3D(z->queue,rep,N,proc) ;
  }
}

void Rep3D(dzone3D **res,int P,int *rep, int N){
  int i ;
  for(i=0;i<P;i++){
    RepZone3D(res[i],rep,N,i) ;
    freeDZone3D(res[i]) ;
  }
}

void RepartFinal(double *frac,int P,int *rep,int N,int Algorithm, int Rounding){
  if(Algorithm == ALG_HILBERT2D || Algorithm == ALG_HILBERT3D){
    int log_N = 0 ; int N_bis =1 ;
    while(N_bis < N){
      log_N++;
      N_bis = N_bis*2 ;
    }
    if(N_bis != N){
      fprintf(stderr, "N no power of 2\n") ;
      exit(EXIT_FAILURE) ;
    }
    if(Algorithm == ALG_HILBERT2D ){
      int *rep2D = malloc(N*N*sizeof(int)) ;
      Hilbert2D(frac,P,rep2D,log_N) ;
      Rep2Dto3D(rep2D,rep,N) ;
      free(rep2D) ;
    }
    if(Algorithm == ALG_HILBERT3D ){
      Hilbert3D(frac,P,rep,log_N) ;
    }   
    return ;
  }
  if(Rounding == BRUTAL_ROUNDING){
    if(Algorithm <= ALG_NRRP){
      dzone **res = malloc(P*sizeof(dzone*)) ;
      int *rep2D = malloc(N*N*sizeof(int)) ;
      if(Algorithm == ALG_CB){
	RoundColumnBased(frac,P,res,N) ;
      }
      if(Algorithm == ALG_RRP){
	RoundRRP(frac,P,res,N) ;
      }
      if(Algorithm == ALG_SNRRP){
	RoundSNRRP(frac,P,res,N) ;
      }
      if(Algorithm == ALG_NRRP){
	RoundNRRP(frac,P,res,N) ;
      }
      Rep2D(res,P,rep2D,N) ;
      Rep2Dto3D(rep2D,rep,N) ;
      free(rep2D) ;
      free(res) ;
    }
    if(Algorithm >= ALG_SCR){
      dzone3D **res = malloc(P*sizeof(dzone3D*)) ;
      if(Algorithm == ALG_SCR){
	RoundSCR(frac,P,res,N) ;
      }
      if(Algorithm == ALG_NRRP3D){
	RoundNRRP3D(frac,P,res,N) ;
      }
      Rep3D(res,P,rep,N) ;
      free(res) ;
    }
    return ;
  }
  if(Rounding == CLEVER_ROUNDING){
    if(Algorithm <= ALG_NRRP){
      zone **res = malloc(P*sizeof(zone*)) ;
      int *rep2D = malloc(N*N*sizeof(int)) ;
      if(Algorithm == ALG_CB){
        ColumnBasedRepart(frac,P,res) ;
      }
      if(Algorithm == ALG_RRP){
        RRP(frac,P,res) ;
      }
      if(Algorithm == ALG_SNRRP){
	SNRRP(frac,P,res) ;
      }
      if(Algorithm == ALG_NRRP){
	NRRP(frac,P,res) ;
      }
      SmartRounding(frac,P,res,N,rep2D) ;
      Rep2Dto3D(rep2D,rep,N) ;
      free(rep2D) ;
      free(res) ;
    }
    if(Algorithm >= ALG_SCR){
      zone3D **res = malloc(P*sizeof(zone3D*)) ;
      if(Algorithm == ALG_SCR){
	SCRRepart(frac,P,res) ;
      }
      if(Algorithm == ALG_NRRP3D){
	NRRP3D(frac,P,res) ;
      }
      SmartRounding3D(frac,P,res,N,rep) ;
      free(res) ;
    }
  }
}


void CheckRep(double *frac,int P,int *rep,int N,int Algorithm, int Rounding, int* counts){
  int *save_volumes = malloc(P*sizeof(int)) ;
  int is2D = Algorithm < ALG_SCR; 
  int is_smart = Rounding == CLEVER_ROUNDING; 
  int v_tot = 0 ;
  double v = 0 ;
  int i,j,k ;
  if(!is2D){
    for(i=0;i<P;i++){
      v = N*N*N*frac[i]+v ;
      save_volumes[i] = rounding(v) - v_tot ;
      v_tot = v_tot + save_volumes[i] ;
    } 
  }
  else{
    for(i=0;i<P;i++){
      v = N*N*frac[i]+v ;
      save_volumes[i] = rounding(v) - v_tot ;
      v_tot = v_tot + save_volumes[i] ;
      save_volumes[i] = save_volumes[i]*N ;
    } 
  }
  if(counts) {
    for(i = 0; i < P; i++) 
      counts[i] = 0; 
  }
  
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      for(k=0;k<N;k++){
  	if(GET3(rep,i,j,k,N) >= 0 && GET3(rep,i,j,k,N) < P){
  	  save_volumes[GET3(rep,i,j,k,N)] -- ;
	  counts[GET3(rep, i, j, k, N)] ++; 
  	}
  	else{
  	  printf("CheckRep: error on indices %d %d %d: %d\n",i,j,k,GET3(rep,i,j,k,N)) ;
  	  exit(EXIT_FAILURE) ;
  	}
      }
    }
  }
  if(is_smart){
    for(i=0;i<P;i++){
      if(save_volumes[i] != 0){
	printf("CheckRep: error on processor %d, volume difference %d\n",i, save_volumes[i]) ;
	exit(EXIT_FAILURE) ;
      }
    }
  }
}
