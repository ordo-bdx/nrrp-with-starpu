#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "rounding.h"
#include "../NRRP_C/rectangle.h"
#include "../NRRP_C/nrrp.h"
#include "../Column_Based/column_based.h"
#include "../NRRP3D_C/cuboid.h"

#define GET2(A,i,j,N) A[(i)+(N)*(j)]
#define GET3(A,i,j,k,N) A[(i)+(N)*(j)+(N)*(N)*k]

int SmartRoundingRectangle(rectangle R,int proc,int N,int *rep){
  int x1 = ceil(N*R.x1) ;
  int x2 = floor(N*R.x2) ;
  int y1 = ceil(N*R.y1) ;
  int y2 = floor(N*R.y2) ;
  int i,j ;
  int w = x2-x1 ; int h = y2-y1 ;
  int s = 0 ;
  if(w > 0 && h>0){
    s= h*w ;
    for(i=x1;i<x2 ;i++){
      for(j=y1;j<y2;j++){
	GET2(rep,i,j,N) = proc ;
      }
    }
  }
  return s ;
}

int SmartRoundingZone(zone *z,int proc,int N,int *rep){
  if(z!=NULL){
    return SmartRoundingRectangle(z->head,proc,N,rep) + SmartRoundingZone(z->queue,proc,N,rep) ;
  }
  else{
    return 0 ;
  }
}

void ChooseProc(int *rep,int i,int j,int N,int *surfaces,int P){
  int proc = -1 ; int min = N*N ;
  int i_min,i_max,j_min,j_max ;
  if(i> 0)
    i_min = i-1 ;
  else
    i_min = i ;
  if(j>0)
    j_min = j-1 ;
  else
    j_min = j ;
  if(i<N-1)
    i_max = i+1 ;
  else
    i_max = i ;
  if(j<N-1)
    j_max = j+1 ;
  else
    j_max = j ;
  int ibis,jbis ;
  int nb_candidat = 0 ;
  for(ibis=i_min;ibis<=i_max;ibis++){
    for(jbis=j_min;jbis<=j_max;jbis++){
      int procbis = GET2(rep,ibis,jbis,N) ;
      if(procbis> -1){
	nb_candidat ++ ;
	if(surfaces[procbis]<min && surfaces[procbis] > 0){
	  proc = procbis ; min = surfaces[procbis] ;
	}
      }
    }
  }
  if(proc > -1){
    GET2(rep,i,j,N) = proc ;
    surfaces[proc] -- ;
  }
  else{   
    min = N*N ;
    int procbis = -1 ;
    int stot = 0 ;
    for(ibis=0;ibis<P;ibis++){
      stot = stot + surfaces[ibis] ; 
      if (surfaces[ibis] < min && surfaces[ibis] > 0){
	min = surfaces[ibis] ; procbis = ibis ;
      }
    }
    GET2(rep,i,j,N) = procbis ;   
    surfaces[procbis]-- ;
  }
}

void SmartRounding(double *surfaces, int P, zone **res,int N,int *rep){
  int i,j ;
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      GET2(rep,i,j,N) = -1 ;
    }
  }
  int *rounded_surfaces = malloc(P*sizeof(int)) ; 
  int s_tot = 0 ;
  double s = 0 ;
  for(i=0;i<P;i++){
    s = N*N*surfaces[i]+s ;
    rounded_surfaces[i] = rounding(s) - s_tot ;
    s_tot = s_tot + rounded_surfaces[i] ;
  } 
  for(i = 0;i<P;i++){
    int s1 = SmartRoundingZone(res[i],i,N,rep) ;
    rounded_surfaces[i] = rounded_surfaces[i] - s1 ;
    s_tot = s_tot - s1 ;
    freeZone(res[i]) ;
  }
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      if(GET2(rep,i,j,N) == -1){
        ChooseProc(rep,i,j,N,rounded_surfaces,P) ;
	s_tot-- ;
      }
    }
  }
  free(rounded_surfaces) ;
}



int SmartRoundingCuboid(cuboid C,int proc,int N,int *rep){
  int x1 = ceil(N*C.x1) ;
  int x2 = floor(N*C.x2) ;
  int y1 = ceil(N*C.y1) ;
  int y2 = floor(N*C.y2) ;
  int z1 = ceil(N*C.z1) ;
  int z2 = floor(N*C.z2) ;
  int i,j,k ;
  int w = x2-x1 ; int h = y2-y1 ; int l = z2-z1 ;
  int v = 0 ;
  if(w > 0 && h>0 && l> 0){
    v= h*w*l ;
    for(i=x1;i<x2 ;i++){
      for(j=y1;j<y2;j++){
	for(k=z1;k<z2;k++){
	  GET3(rep,i,j,k,N) = proc ;
	}
      }
    }
  }
  return v ;
}


int SmartRoundingZone3D(zone3D *z,int proc,int N,int *rep){
  if(z!=NULL){
    return SmartRoundingCuboid(z->head,proc,N,rep) + SmartRoundingZone3D(z->queue,proc,N,rep) ;
  }
  else{
    return 0 ;
  }
}



void ChooseProc3D(int *rep,int i,int j,int k,int N,int *volumes,int P){
  int proc = -1 ; int min = N*N ;
  int i_min,i_max,j_min,j_max,k_min,k_max ;
  if(i> 0)
    i_min = i-1 ;
  else
    i_min = i ;
  if(j>0)
    j_min = j-1 ;
  else
    j_min = j ;
  if(i<N-1)
    i_max = i+1 ;
  else
    i_max = i ;
  if(j<N-1)
    j_max = j+1 ;
  else
    j_max = j ;
  if(k> 0)
    k_min = k-1 ;
  else
    k_min = k ; 
  if(k<N-1)
    k_max = k+1 ;
  else
    k_max = k ;
  int ibis,jbis,kbis ;
  for(ibis=i_min;ibis<=i_max;ibis++){
    for(jbis=j_min;jbis<=j_max;jbis++){
      for(kbis=k_min;kbis<=k_max;kbis++){
	int procbis = GET3(rep,ibis,jbis,kbis,N) ;
	if(procbis> -1){
	  if(volumes[procbis]<min && volumes[procbis] > 0){
	    proc = procbis ; min = volumes[procbis] ;
	  }
	}
      }
    }
  }
  if(proc > -1){
    GET3(rep,i,j,k,N) = proc ;
    volumes[proc] -- ;
  }
  else{   
    min = N*N*N ;
    int procbis = -1 ;
    for(ibis=0;ibis<P;ibis++){
      if (volumes[ibis] < min && volumes[ibis] > 0){
	min = volumes[ibis] ; procbis = ibis ;
      }
    }
    GET3(rep,i,j,k,N) = procbis ;   
    volumes[procbis]-- ;
  }
}


void SmartRounding3D(double *volumes, int P, zone3D **res,int N,int *rep){
  int i,j,k ;
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      for(k=0;k<N;k++){
	GET3(rep,i,j,k,N) = -1 ;
      }
    }
  }
  int *rounded_volumes = malloc(P*sizeof(int)) ; 
  int v_tot = 0 ;
  double v = 0 ;
  for(i=0;i<P;i++){
    v = N*N*N*volumes[i]+v ;
    rounded_volumes[i] = rounding(v) - v_tot ;
    v_tot = v_tot + rounded_volumes[i] ;
  } 
  for(i = 0;i<P;i++){
    int v1 = SmartRoundingZone3D(res[i],i,N,rep) ;
    rounded_volumes[i] = rounded_volumes[i] - v1 ;
    v_tot = v_tot - v1 ;
    freeZone3D(res[i]) ;
  }
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      for(k=0;k<N;k++){
	if(GET3(rep,i,j,k,N) == -1){
	  ChooseProc3D(rep,i,j,k,N,rounded_volumes,P) ;
	  v_tot-- ;
	}
      }
    }
  }
  free(rounded_volumes) ;
}
