#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../NRRP_C/rectangle.h"
#include "../NRRP_C/nrrp.h"
#include "../Column_Based/column_based.h"
#include "../NRRP3D_C/nrrp.h"

int rounding(double d){
  int n = floor(d) ;
  if(d-n < 0.5){
    return n ;
  }
  else{
    return n+1 ;
  }
}

drectangle RoundRectangle(rectangle R,int N){
  drectangle DR ;
  DR.x1 = rounding(N*R.x1) ;
  DR.x2 = rounding(N*R.x2) ;
  DR.y1 = rounding(N*R.y1) ;
  DR.y2 = rounding(N*R.y2) ;
  return DR ;
}

dzone *RoundZone(zone *z,int N){
  dzone *dz = malloc(sizeof(dzone)) ;
  if(z==NULL){
    return NULL ;
  }
  else{
    dz -> head = RoundRectangle(z -> head,N) ;
    dz -> queue = RoundZone(z->queue,N) ;
  }
  return dz ;
}

void RoundNRRP(double *surfaces,int P, dzone **res,int N){
  zone ** res_bis = malloc(P*sizeof(zone*)) ;
  NRRP(surfaces,P,res_bis) ;
  int i ;
  for(i=0;i<P;i++){
    res[i] = RoundZone(res_bis[i],N) ;
    freeZone(res_bis[i]) ;
  }
  free(res_bis) ;
}

void RoundSNRRP(double *surfaces,int P, dzone **res,int N){
  zone ** res_bis = malloc(P*sizeof(zone*)) ;
  SNRRP(surfaces,P,res_bis) ;
  int i ;
  for(i=0;i<P;i++){
    res[i] = RoundZone(res_bis[i],N) ;
    freeZone(res_bis[i]) ;
  }
  free(res_bis) ;
}

void RoundRRP(double *surfaces,int P, dzone **res,int N){
  zone ** res_bis = malloc(P*sizeof(zone*)) ;
  RRP(surfaces,P,res_bis) ;
  int i ;
  for(i=0;i<P;i++){
    res[i] = RoundZone(res_bis[i],N) ;
    freeZone(res_bis[i]) ;
  }
  free(res_bis) ;
}

void RoundColumnBased(double *surfaces,int P, dzone **res,int N){
  zone ** res_bis = malloc(P*sizeof(zone*)) ;
  ColumnBasedRepart(surfaces,P,res_bis) ;
  int i ;
  for(i=0;i<P;i++){
    res[i] = RoundZone(res_bis[i],N) ;
    freeZone(res_bis[i]) ;
  }
  free(res_bis) ;
}

dcuboid RoundCuboid(cuboid C,int N){
  dcuboid DC ;
  DC.x1 = rounding(N*C.x1) ;
  DC.x2 = rounding(N*C.x2) ;
  DC.y1 = rounding(N*C.y1) ;
  DC.y2 = rounding(N*C.y2) ;
  DC.z1 = rounding(N*C.z1) ;
  DC.z2 = rounding(N*C.z2) ;
  return DC ;
}

dzone3D *RoundZone3D(zone3D *z,int N){
  dzone3D *dz = malloc(sizeof(dzone3D)) ;
  if(z==NULL){
    return NULL ;
  }
  else{
    dz -> head = RoundCuboid(z -> head,N) ;
    dz -> queue = RoundZone3D(z->queue,N) ;
  }
  return dz ;
}

void RoundNRRP3D(double *volumes,int P, dzone3D **res,int N){
  zone3D ** res_bis = malloc(P*sizeof(zone3D*)) ;
  NRRP3D(volumes,P,res_bis) ;
  int i ;
  for(i=0;i<P;i++){
    res[i] = RoundZone3D(res_bis[i],N) ;
    freeZone3D(res_bis[i]) ;
  }
  free(res_bis) ;
}

void RoundSCR(double *volumes,int P, dzone3D **res,int N){
  zone3D ** res_bis = malloc(P*sizeof(zone3D*)) ;
  SCRRepart(volumes,P,res_bis) ;
  int i ;
  for(i=0;i<P;i++){
    res[i] = RoundZone3D(res_bis[i],N) ;
    freeZone3D(res_bis[i]) ;
  }
  free(res_bis) ;
}
