#ifndef ROUNDING
#define ROUNDING

#include "../NRRP_C/rectangle.h"
#include "../NRRP3D_C/cuboid.h"

int rounding(double d);

void RoundNRRP(double *surfaces,int P, dzone **res,int N) ;
void RoundRRP(double *surfaces,int P, dzone **res, int N) ;
void RoundSNRRP(double *surfaces,int P, dzone **res, int N) ;
void RoundColumnBased(double *surfaces,int P, dzone **res,int N) ;

void RoundNRRP3D(double *volumes,int P, dzone3D **res,int N) ;
void RoundSCR(double *volumes,int P, dzone3D **res,int N) ;

#endif
