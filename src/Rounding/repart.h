#ifndef REPART
#define REPART

void RepartFinal(double *frac,int P,int *rep,int N,int Algorithm, int Rounding) ; //given the respective speeds of the processors (frac, the sum must be one), their number P, the size of the matrix N, the wanted partitioning algorithm and the way to round, return in rep (NxNxN array) the processors allocation. The possible algorithms are:  
                  //ALG_CB -> ColumnBased
                  //ALG_RRP -> RRP
                  //ALG_SNRRP -> SNRRP
                  //ALG_NRRP -> NRRP
                  //ALG_HILBERT2D -> SFCP
                  //ALG_SCR -> SCR
                  //ALG_NRRP3D -> 3D-NRRP
                  //ALG_HILBERT3D -> 3D-SFCP
//The rounding modes are:
                  //BRUTAL_ROUNDING -> Rounding
                  //CLEVER_ROUNDING 1 -> Precise

void CheckRep(double *frac,int P,int *rep,int N,int Algorithm, int Rounding, int* counts) ; //function use to check the consistency and the load-balancing of the repartition
#endif
