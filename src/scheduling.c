#include <starpu.h>
#include <starpu_scheduler.h>
#include "scheduling.h"
#include "time.h"
#include "codelet.h"

int DELAY_BEFORE_PREFETCH = 2;
int NUMBER_SECOND_CHOICE_STEAL = 3;
int NUMBER_LOOKED_TASKS = 10;

#define FPRINTF(ofile, fmt, ...) do { if (!getenv("STARPU_SSILENT")) {fprintf(ofile, fmt, ## __VA_ARGS__); }} while(0)


int thomas_scheduling_initialized = 0; 

void init_sched(unsigned sched_ctx_id)
{

#ifndef PARALLEL_TASKS
  STARPU_ASSERT(thomas_scheduling_initialized == 0); 
#endif
  thomas_scheduling_initialized = 1; 

  if(STARPU_MINOR_VERSION <= 2) 
    starpu_sched_ctx_create_worker_collection(sched_ctx_id, STARPU_WORKER_LIST) ;
  

  DELAY_BEFORE_PREFETCH = starpu_get_env_number_default("DELAY_BEFORE_PREFETCH", DELAY_BEFORE_PREFETCH); 
  NUMBER_LOOKED_TASKS = starpu_get_env_number_default("NUMBER_LOOKED_TASKS", NUMBER_LOOKED_TASKS); 
  NUMBER_SECOND_CHOICE_STEAL = starpu_get_env_number_default("NUMBER_SECOND_CHOICE_STEAL", NUMBER_SECOND_CHOICE_STEAL); 

  struct sched_data_struct *data_struct = malloc(sizeof(struct sched_data_struct)) ;
  struct sched_data *data = (struct sched_data*)malloc(STARPU_NMAXWORKERS*sizeof(struct sched_data));
  data_struct->data = data ;
  data_struct->nb_memnode = 0 ;
  data_struct->no_alloc = 0 ;
  data_struct->nb_no_alloc_task_in_queue = 0 ;
  data_struct->id_memnode = malloc(STARPU_NMAXWORKERS*sizeof(int)) ;
  starpu_task_list_init(&(data_struct)->sched_list_no_alloc);
  starpu_pthread_mutex_init(&(data_struct)->no_alloc_mutex, NULL); 


  int i ;
  for(i=0;i<STARPU_NMAXWORKERS;i++){
    data[i].is_init = 0 ;
  }
    
  starpu_sched_ctx_set_policy_data(sched_ctx_id, (void*)data_struct);
   

}

void add_workers(unsigned sched_ctx_id, int *workerids, unsigned nworkers){
  struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(sched_ctx_id);
  struct sched_data *data = data_struct->data ;
  int workerid,memnode;
  unsigned i;
  for (i = 0; i < nworkers; i++){
    workerid = workerids[i];
    memnode = starpu_worker_get_memory_node(workerid) ;
    if(!data[memnode].is_init){
      data[memnode].is_init = 1 ;
      starpu_task_list_init(&(data+memnode)->sched_list);
      starpu_pthread_mutex_init(&(data+memnode)->policy_mutex, NULL); 
      data[memnode].has_began = 0 ;
      data[memnode].nb_task_in_queue = 0; 
      data_struct->id_memnode[data_struct->nb_memnode] = memnode ;
      data_struct->nb_memnode ++ ;
    }
  }
}

void deinit_sched(unsigned sched_ctx_id)
{
  struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(sched_ctx_id);
  struct sched_data *data = data_struct->data ;
  int nb_proc = starpu_worker_get_count() ;
  int i,memnode ;
  for(i=0; i < nb_proc; i++){
    memnode = starpu_worker_get_memory_node(i) ;
    STARPU_ASSERT(starpu_task_list_empty(&(data+memnode)->sched_list));
    if(data[memnode].is_init != -1) {
      data[memnode].is_init = -1; 
      
      starpu_pthread_mutex_destroy(&(data+memnode)->policy_mutex);
    }
    
  }
  
  STARPU_ASSERT(starpu_task_list_empty(&(data_struct)->sched_list_no_alloc));
  starpu_pthread_mutex_destroy(&(data_struct)->no_alloc_mutex);
  
  if(STARPU_MINOR_VERSION <= 2) 
    starpu_sched_ctx_delete_worker_collection(sched_ctx_id);
  
  free(data) ;
  free(data_struct->id_memnode) ;
  free(data_struct) ;
  
}

int get_memnode(struct starpu_task* task) {
  int i, j, k, memnode; 
  if(!task)
    return -1; 
  if (task->cl == &cl || task->cl == &cl_redux){ // This is a GEMM task
    starpu_codelet_unpack_args(task->cl_arg, &i, &j, &k, &memnode) ;
    return memnode; 
  }
  return -1; 
}

void wake_all_workers(unsigned sched_ctx_id) {
  unsigned worker = 0;
  struct starpu_worker_collection *workers = starpu_sched_ctx_get_worker_collection(sched_ctx_id);
  
  struct starpu_sched_ctx_iterator it;
  workers->init_iterator(workers, &it);
  while(workers->has_next(workers, &it))
    {
      worker = workers->get_next(workers, &it);
      starpu_pthread_mutex_t *sched_mutex;
      starpu_pthread_cond_t *sched_cond;
      starpu_worker_get_sched_condition(worker, &sched_mutex, &sched_cond);
      starpu_pthread_mutex_lock(sched_mutex);
      starpu_pthread_cond_signal(sched_cond);
      starpu_pthread_mutex_unlock(sched_mutex);
    }
}


int push_task(struct starpu_task *task)
{ 
  unsigned sched_ctx_id = task->sched_ctx;
  struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(sched_ctx_id);
  struct sched_data *data = data_struct->data ;
	/* NB: In this simplistic strategy, we assume that the context in which
	   we push task has at least one worker*/
  if(data_struct->no_alloc){ 
    starpu_pthread_mutex_lock(&(data_struct)->no_alloc_mutex);
    starpu_task_list_push_back(&(data_struct)->sched_list_no_alloc, task); 
    starpu_push_task_end(task); 
    data_struct->nb_no_alloc_task_in_queue ++ ;
    starpu_pthread_mutex_unlock(&(data_struct)->no_alloc_mutex);
    
    wake_all_workers(sched_ctx_id); 

    return 0;
  }
  
  
  int nb_memnode = data_struct->nb_memnode ;
  int memnode = get_memnode(task); 
  if(memnode < 0) { 
    // If the task was not pre-allocated, we assign it 
    // to any memory node which owns one handle. 
    int i; 
    for(i = 0; i < nb_memnode; i++) {
      memnode = i; 
      int is_valid; 
      starpu_data_query_status(task->handles[1], memnode, NULL, &is_valid, NULL) ;
      if(is_valid)
	break;
    }
  }
  
  if(memnode >= nb_memnode) {
    static int warned = 0; 
    if(!warned) {
      printf("Warn: could not find a memnode\n"); 
      warned = 1; 
    }

    memnode = 0; 
  }

  /* push task on the list of the corresponding memnode */
  starpu_pthread_mutex_lock(&(data+memnode)->policy_mutex);
  starpu_task_list_push_back(&(data+memnode)->sched_list, task);   
  starpu_push_task_end(task);
  data[memnode].nb_task_in_queue ++ ;
  if(data[memnode].nb_task_in_queue <= DELAY_BEFORE_PREFETCH && starpu_get_prefetch_flag()){
    starpu_prefetch_task_input_on_node(task,memnode);
  }
  starpu_pthread_mutex_unlock(&(data+memnode)->policy_mutex);
  
  wake_all_workers(sched_ctx_id); 

  return 0;
}

/* Assumes that there are at least n-1 tasks in the list */
static inline struct starpu_task* get_nth_task_from_list(struct starpu_task_list *list, int n) {
  struct starpu_task *task; 
  int i ;
  task = starpu_task_list_front(list) ;
  for(i = 1; i < n; i++){
    task = starpu_task_list_next(task) ;
  }
  return task; 
}

/* Available means not reserved (ie not already prefetched) by the
   owner memnode. This function is meant to be used to get a task that
   can be stolen */
static inline struct starpu_task* pop_available_task(struct sched_data *data, int memnode) {
  struct starpu_task* task = NULL; 
  if(data[memnode].nb_task_in_queue <= DELAY_BEFORE_PREFETCH)
    return NULL; 
  starpu_pthread_mutex_lock(&(data+memnode)->policy_mutex);
  if(data[memnode].nb_task_in_queue > DELAY_BEFORE_PREFETCH){
    task = get_nth_task_from_list(&(data+memnode)->sched_list, DELAY_BEFORE_PREFETCH);
    if(task) {
      starpu_task_list_erase(&(data+memnode)->sched_list,task) ;
      data[memnode].nb_task_in_queue -- ;
    }
  }
  starpu_pthread_mutex_unlock(&(data+memnode)->policy_mutex);
  return task; 
}

static inline struct starpu_task* peek_available_task(struct sched_data *data, int memnode) {
  struct starpu_task* task = NULL; 
  if(data[memnode].nb_task_in_queue <= DELAY_BEFORE_PREFETCH)
    return NULL; 
  starpu_pthread_mutex_lock(&(data+memnode)->policy_mutex);
  if(data[memnode].nb_task_in_queue > DELAY_BEFORE_PREFETCH){
    task = get_nth_task_from_list(&(data+memnode)->sched_list, DELAY_BEFORE_PREFETCH);
  }
  starpu_pthread_mutex_unlock(&(data+memnode)->policy_mutex);
  return task; 
}

/* This function is used at the beginning of almost all pop()
   scheduling functions */
static inline struct starpu_task* pop_first_prefetch_next(struct sched_data * data, int memnode, int* did_prefetch) {
  struct starpu_task *task = NULL ;
  struct starpu_task *task_prefetch = NULL; 
  if(did_prefetch) *did_prefetch = 0; 
  if(data[memnode].nb_task_in_queue == 0)
    return NULL; 
  starpu_pthread_mutex_lock(&(data+memnode)->policy_mutex);
  if(data[memnode].nb_task_in_queue > 0){
    task = starpu_task_list_pop_front(&(data+memnode)->sched_list);
    data[memnode].nb_task_in_queue -- ;
    if(data[memnode].nb_task_in_queue >= DELAY_BEFORE_PREFETCH && data[memnode].nb_task_in_queue>0 && starpu_get_prefetch_flag()){     
      task_prefetch = get_nth_task_from_list(&(data+memnode)->sched_list, DELAY_BEFORE_PREFETCH) ;
    }
  }
  starpu_pthread_mutex_unlock(&(data+memnode)->policy_mutex);
  if(task_prefetch) {
    starpu_prefetch_task_input_on_node(task_prefetch,memnode);
    if(did_prefetch) *did_prefetch = 1; 
  }
  return task; 
}

/* */
static inline void insert_locally_and_prefetch(struct sched_data * data, int memnode, struct starpu_task* task) {
  starpu_pthread_mutex_lock(&(data+memnode)->policy_mutex);	
  starpu_task_list_push_back(&(data+memnode)->sched_list, task); 
  data[memnode].nb_task_in_queue ++ ;
  starpu_pthread_mutex_unlock(&(data+memnode)->policy_mutex);
  if(starpu_get_prefetch_flag()){
    starpu_prefetch_task_input_on_node(task,memnode);
  }
}

/* The mutex associated to the calling worker is already taken by StarPU */
struct starpu_task *pop_task_no_steal(unsigned sched_ctx_id)
{
  int proc = starpu_worker_get_id() ;
  int memnode = starpu_worker_get_memory_node(proc) ;
  struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(sched_ctx_id);
  struct sched_data *data = data_struct->data ;

  return pop_first_prefetch_next(data, memnode, NULL); 
}

struct starpu_task *pick_random_task(struct sched_data_struct *struct_data){
  int nb_memnode = struct_data->nb_memnode ;
  struct sched_data *data = struct_data->data ;
  int i = rand() % nb_memnode ;
  int memnode = struct_data->id_memnode[i] ;
  int memnode_dep = memnode; 
  struct starpu_task *task = NULL ;
  
  do {
    task = pop_available_task(data, memnode); 
    i = (i+1) % nb_memnode; 
    memnode = struct_data->id_memnode[i]; 
  } while(task == NULL && memnode != memnode_dep);

  return task ;
}

struct starpu_task *pop_task_rand_steal(unsigned sched_ctx_id)
{
  int proc = starpu_worker_get_id();
  int memnode = starpu_worker_get_memory_node(proc) ;
  struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(sched_ctx_id);
  struct sched_data *data = data_struct->data ;
  struct starpu_task *task = NULL ;
  int did_prefetch = 0; 
  
  task = pop_first_prefetch_next(data, memnode, &did_prefetch); 
    
  if((task == NULL && data[memnode].has_began)
     || (!did_prefetch && starpu_get_prefetch_flag())) {
    struct starpu_task *stolen_task = pick_random_task(data_struct);
    if(stolen_task) {
      if(task) {
	insert_locally_and_prefetch(data, memnode, stolen_task); 
      }
      else{
	return stolen_task ;
      }
    }
  }
  return task; 
}

struct starpu_task *pop_task_loaded_steal(unsigned sched_ctx_id)
{
  int proc = starpu_worker_get_id();
  int memnode = starpu_worker_get_memory_node(proc) ;
  struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(sched_ctx_id);
  struct sched_data *data = data_struct->data ;
  struct starpu_task *task = NULL ;
  int did_prefetch = 0; 

  task = pop_first_prefetch_next(data, memnode, &did_prefetch); 

  if((task == NULL && data[memnode].has_began)
     || (!did_prefetch && starpu_get_prefetch_flag())) {
    int i ;
    int nb_memnode = data_struct->nb_memnode ;   
    int stolen_memnode ;
    stolen_memnode = data_struct->id_memnode[0] ;
    for(i=1;i<nb_memnode;i++){
      if(data[stolen_memnode].nb_task_in_queue < data[data_struct->id_memnode[i]].nb_task_in_queue){
	stolen_memnode = data_struct->id_memnode[i] ;
      }
    }

    if(stolen_memnode != memnode && data[stolen_memnode].nb_task_in_queue >= DELAY_BEFORE_PREFETCH && 
       DELAY_BEFORE_PREFETCH > 0 && starpu_get_prefetch_flag()){ 
      struct starpu_task *stolen_task = pop_available_task(data, memnode);
      if(stolen_task) {
	if(task){
	  insert_locally_and_prefetch(data, memnode, stolen_task); 
	} else {
	  return stolen_task ;
	}
      }
    }
  }
  return task; 
}


int get_cost(struct starpu_task *task,int memnode){
  if (task == NULL){ 
    return 4 ;
  }
  else{
    int cost = 0 ;
    int is_valid = 0 ;
    int is_allocated = 0 ;
    int i ;
    for(i=0;i<task->cl->nbuffers;i++){
      starpu_data_query_status(task->handles[i],memnode,&is_allocated,&is_valid,NULL) ;
      cost = cost + (is_valid ? 0 : 1) ;
    }

    return cost ;
  }
}

struct starpu_task *pop_task_rand_choice_steal(unsigned sched_ctx_id)
{
  int proc = starpu_worker_get_id();
  int memnode = starpu_worker_get_memory_node(proc) ;
  struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(sched_ctx_id);
  struct sched_data *data = data_struct->data ;
  struct starpu_task *task = NULL ;

  int did_prefetch = 0; 

  task = pop_first_prefetch_next(data, memnode, &did_prefetch); 

  if((task == NULL && data[memnode].has_began)
     || (!did_prefetch && starpu_get_prefetch_flag())) {
    int i ;
    int nb_memnode = data_struct->nb_memnode ; 
    int *choice = malloc(nb_memnode*sizeof(int)) ;
    i = rand()%nb_memnode ;
    int i_dep = i ;
    int j = 0;
    do{
      if(data_struct->id_memnode[i] != memnode) {
	choice[j] = data_struct->id_memnode[i] ;
	j ++ ;
      }
      i = (i+1)%nb_memnode ;
    } while(i != i_dep) ;

    struct starpu_task *stolen_task = NULL ;
    int min = 4; 
    int cost ;
    int actual_min = 0 ;
    for(i = 0; i < nb_memnode - 1; i++){
      stolen_task = peek_available_task(data, choice[i]); 
      cost = get_cost(stolen_task, memnode);
      if(min > cost) {
	min = cost ;
	actual_min = i ;
      }
    }
    if(min < 4){
      // Get task to be stolen 
      int victim = choice[actual_min]; 
      stolen_task = pop_available_task(data, victim); 
      free(choice) ;
      
      if(stolen_task){
	// Here we insert stolen_task in the local queue instead of returning it
	if(task){
	  insert_locally_and_prefetch(data, memnode, stolen_task); 
	} else {
	  return stolen_task ;
	}
      }
    } else {
      free(choice) ;
    }
  }
  return task ;
}


struct starpu_task *pop_task_cheaper_steal(unsigned sched_ctx_id)
{
  int proc = starpu_worker_get_id();
  int memnode = starpu_worker_get_memory_node(proc) ;
  struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(sched_ctx_id);
  struct sched_data *data = data_struct->data ;
  struct starpu_task *task = NULL ;
  int did_prefetch = 0; 

  task = pop_first_prefetch_next(data, memnode, &did_prefetch); 

  if((task == NULL && data[memnode].has_began)
     || (!did_prefetch && starpu_get_prefetch_flag())) {
    
    int nb_memnode = data_struct->nb_memnode ;   
    int memnode_1[NUMBER_SECOND_CHOICE_STEAL] ;
    int nb_memnode_1 = 0 ;
    int memnode_2[NUMBER_SECOND_CHOICE_STEAL] ;
    int nb_memnode_2 = 0 ;
    int other_memnode = 0 ;
    int i = rand()%nb_memnode ;
    int i_dep = i ;
    struct starpu_task *stolen_task = NULL ;
    do{
      other_memnode = data_struct->id_memnode[i] ;
      starpu_pthread_mutex_lock(&(data+other_memnode)->policy_mutex);
      if(data[other_memnode].nb_task_in_queue>DELAY_BEFORE_PREFETCH){
	stolen_task = starpu_task_list_front(&(data+other_memnode)->sched_list) ;
	int j ;
	for(j=0;j<DELAY_BEFORE_PREFETCH;j++){
	  stolen_task = starpu_task_list_next(stolen_task) ;
	}

	while(stolen_task != NULL && get_cost(stolen_task,memnode) > 0 ){
	  if(get_cost(stolen_task,memnode) == 1 && (nb_memnode_1 == 0 || memnode_1[nb_memnode_1-1] != other_memnode) 
	     && nb_memnode_1 < NUMBER_SECOND_CHOICE_STEAL){
	    memnode_1[nb_memnode_1] = other_memnode ;
	    nb_memnode_1 ++ ;
	  }
	  if(get_cost(stolen_task,memnode) == 2 && (nb_memnode_2 == 0 || memnode_2[nb_memnode_2-1] != other_memnode) 
	     && nb_memnode_2 < NUMBER_SECOND_CHOICE_STEAL){
	    memnode_2[nb_memnode_2] = other_memnode ;
	    nb_memnode_2 ++ ;
	  }
	  stolen_task = starpu_task_list_next(stolen_task) ;
	}

	if(stolen_task != NULL && get_cost(stolen_task,memnode) == 0){
	  starpu_task_list_erase(&(data+other_memnode)->sched_list,stolen_task) ;
	  data[other_memnode].nb_task_in_queue -- ;
	}
      }
      starpu_pthread_mutex_unlock(&(data+other_memnode)->policy_mutex);
      i ++ ;
      i = i%nb_memnode ;
    } while(i != i_dep  && stolen_task == NULL) ;

    // Do I have stolen a 0-cost task ?
    if(stolen_task != NULL){
      if(task){
	insert_locally_and_prefetch(data, memnode, stolen_task); 
	return task; 
      }else{
	return stolen_task ;
      }
    }
    
    // Search for a 1-cost task
    i = 0;
    while(i<nb_memnode_1 && stolen_task == NULL){
      other_memnode = memnode_1[i] ;
      starpu_pthread_mutex_lock(&(data+other_memnode)->policy_mutex);  
      if(data[other_memnode].nb_task_in_queue>DELAY_BEFORE_PREFETCH){
	stolen_task = starpu_task_list_front(&(data+other_memnode)->sched_list) ;
	int j ;
	for(j=0;j<DELAY_BEFORE_PREFETCH;j++){
	  stolen_task = starpu_task_list_next(stolen_task) ;
	}
	  while(stolen_task != starpu_task_list_back(&(data+other_memnode)->sched_list) && get_cost(stolen_task,memnode) > 1 ){
	    stolen_task = starpu_task_list_next(stolen_task) ;
	  }   
	  if(stolen_task != NULL && get_cost(stolen_task,memnode)== 1){
	    starpu_task_list_erase(&(data+other_memnode)->sched_list,stolen_task) ;
	    data[other_memnode].nb_task_in_queue -- ;
	  }
	  else{
	    stolen_task = NULL ;
	  }
	}
	starpu_pthread_mutex_unlock(&(data+other_memnode)->policy_mutex);
	i++ ;
      }
      if(stolen_task != NULL){
	if(task){
	  insert_locally_and_prefetch(data, memnode, stolen_task); 
	  return task; 
	} else{
	  return stolen_task ;
	}   
      }
      
      // Search for a 2-cost task
      i = 0 ;
      while(i<nb_memnode_2 && stolen_task == NULL){
	other_memnode = memnode_2[i] ;
	starpu_pthread_mutex_lock(&(data+other_memnode)->policy_mutex);
	if(data[other_memnode].nb_task_in_queue>DELAY_BEFORE_PREFETCH){
	  stolen_task = starpu_task_list_front(&(data+other_memnode)->sched_list) ;
	  int j ;
	  for(j=0;j<DELAY_BEFORE_PREFETCH;j++){
	    stolen_task = starpu_task_list_next(stolen_task) ;
	  }
	  while(stolen_task != starpu_task_list_back(&(data+other_memnode)->sched_list) && get_cost(stolen_task,memnode) > 2 ){
	    stolen_task = starpu_task_list_next(stolen_task) ;
	  }   
	  if(stolen_task != NULL && get_cost(stolen_task,memnode) == 2 ){
	    starpu_task_list_erase(&(data+other_memnode)->sched_list,stolen_task) ;
	    data[other_memnode].nb_task_in_queue -- ;
	  }
	  else{
	    stolen_task = NULL ;
	  }
	}
	starpu_pthread_mutex_unlock(&(data+other_memnode)->policy_mutex);
	i++ ;
      }
      if(stolen_task != NULL){
	if(task){
	  insert_locally_and_prefetch(data, memnode, stolen_task); 
	  return task; 
	} else{
	  return stolen_task ;
	}   
      }
      
      // Pick a random task
      stolen_task = pick_random_task(data_struct);
      if(stolen_task == NULL){
	data[memnode].has_began = 0;
	return task; 
      } else {
	if(task){
	  insert_locally_and_prefetch(data, memnode, stolen_task); 
	  return task; 
	} else{
	  return stolen_task ;
	}
      }
  }
  return task; 
}


struct starpu_task *pop_task_eager_no_alloc(unsigned sched_ctx_id){
  int proc = starpu_worker_get_id();
  int memnode = starpu_worker_get_memory_node(proc) ;
  struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(sched_ctx_id);
  struct sched_data *data = data_struct->data ;
  struct starpu_task *task = NULL ;
  if(data_struct->nb_no_alloc_task_in_queue == 0 && data[memnode].nb_task_in_queue == 0){
    return NULL ;
  } else if(data_struct->nb_no_alloc_task_in_queue == 0){
    starpu_pthread_mutex_lock(&(data+memnode)->policy_mutex);
    task = starpu_task_list_pop_front(&(data+memnode)->sched_list);
    if(task){
      data[memnode].nb_task_in_queue -- ;
    }
    starpu_pthread_mutex_unlock(&(data+memnode)->policy_mutex);
    return task ;
  } else { // Global queue is not empty
    if(DELAY_BEFORE_PREFETCH == 0 || starpu_worker_get_type(proc)==STARPU_CPU_WORKER){
      starpu_pthread_mutex_lock(&(data_struct)->no_alloc_mutex);
      if(data_struct->nb_no_alloc_task_in_queue > 0) {
	task = starpu_task_list_pop_front(&(data_struct)->sched_list_no_alloc);
	data_struct->nb_no_alloc_task_in_queue -- ;
      }
      starpu_pthread_mutex_unlock(&(data_struct)->no_alloc_mutex);
      return task ;
    } else { // I am a GPU and I need to prefetch
      int nb_required_task = (DELAY_BEFORE_PREFETCH+1) - data[memnode].nb_task_in_queue; 
      if(nb_required_task > 0) {
	struct starpu_task **tasks = malloc(nb_required_task * sizeof(struct starpu_task*)) ;
	int i ;
	starpu_pthread_mutex_lock(&(data_struct)->no_alloc_mutex);
	for(i = 0; i < nb_required_task; i++){
	  if(data_struct->nb_no_alloc_task_in_queue >0){
	    tasks[i] = starpu_task_list_pop_front(&(data_struct)->sched_list_no_alloc);
	    data_struct->nb_no_alloc_task_in_queue -- ;
	  }
	  else{
	    tasks[i] = NULL ;
	  }
	}
	starpu_pthread_mutex_unlock(&(data_struct)->no_alloc_mutex);
	if(starpu_get_prefetch_flag()){
	  for(i=0; i < nb_required_task; i++){
	    if(tasks[i]) 
	      starpu_prefetch_task_input_on_node(tasks[i],memnode) ;
	  }
	}
	starpu_pthread_mutex_lock(&(data+memnode)->policy_mutex);	
	for(i = 0; i < nb_required_task; i++){
	  if(tasks[i]){
	    starpu_task_list_push_front(&(data+memnode)->sched_list,tasks[i]);
	    data[memnode].nb_task_in_queue ++ ;
	  }
	}
	starpu_pthread_mutex_unlock(&(data+memnode)->policy_mutex);
      }
      // Now the local list is long enough (or as long as we can make it)
      if(data[memnode].nb_task_in_queue > 0) {
	starpu_pthread_mutex_lock(&(data+memnode)->policy_mutex);
	if(data[memnode].nb_task_in_queue > 0) {
	  task = starpu_task_list_pop_back(&(data+memnode)->sched_list);
	  data[memnode].nb_task_in_queue -- ;
	}
	starpu_pthread_mutex_unlock(&(data+memnode)->policy_mutex);
      }
      return task ;
    }
  }
}

static inline struct starpu_task* pop_best_choice_task(struct sched_data_struct *data_struct, int memnode, int choice_limit) {
  struct starpu_task *task = NULL ;
  struct starpu_task * min_task = NULL; 
  int cost, min, i; 
  if(data_struct->nb_no_alloc_task_in_queue == 0) return NULL; 
  starpu_pthread_mutex_lock(&(data_struct)->no_alloc_mutex);
  i = 1; 
  task =  starpu_task_list_front(&(data_struct)->sched_list_no_alloc) ;
  cost = get_cost(task,memnode) ;
  min_task = task ;
  min = cost ;
  while(task != NULL && i < choice_limit && cost != 0){
    i++ ;
    task = starpu_task_list_next(task) ;
    cost =  get_cost(task, memnode) ;
    if(cost < min){
      min_task = task ;
      min = cost ;
    }
  }
  if(min_task){
    starpu_task_list_erase(&(data_struct)->sched_list_no_alloc, min_task) ;
    data_struct->nb_no_alloc_task_in_queue -- ;	
  }
  starpu_pthread_mutex_unlock(&(data_struct)->no_alloc_mutex);
  return min_task ;
}


struct starpu_task *_pop_task_limited_choice_no_alloc(unsigned sched_ctx_id, int number_choice){
  int proc = starpu_worker_get_id();
  int memnode = starpu_worker_get_memory_node(proc) ;
  struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(sched_ctx_id);
  struct sched_data *data = data_struct->data ;
  struct starpu_task *task = NULL ;
  if(data_struct->nb_no_alloc_task_in_queue == 0 && data[memnode].nb_task_in_queue == 0){
    return NULL ;
  }
  else if(data_struct->nb_no_alloc_task_in_queue == 0){
    starpu_pthread_mutex_lock(&(data+memnode)->policy_mutex);
    if(data[memnode].nb_task_in_queue > 0) {
      task = starpu_task_list_pop_front(&(data+memnode)->sched_list);
      data[memnode].nb_task_in_queue -- ;
    }
    starpu_pthread_mutex_unlock(&(data+memnode)->policy_mutex);
    return task ;
  } 
  else{
    if(DELAY_BEFORE_PREFETCH == 0 || starpu_worker_get_type(proc) == STARPU_CPU_WORKER){
      return(pop_best_choice_task(data_struct, memnode, number_choice)); 
    }
    else{
      while(data[memnode].nb_task_in_queue < DELAY_BEFORE_PREFETCH && data_struct->nb_no_alloc_task_in_queue > 0) {
	struct starpu_task* min_task = pop_best_choice_task(data_struct, memnode, number_choice); 
	
	if(min_task) {
	  insert_locally_and_prefetch(data, memnode, min_task); 
	}
      }
      
      starpu_pthread_mutex_lock(&(data+memnode)->policy_mutex);
      if(data[memnode].nb_task_in_queue > 0) {
	task = starpu_task_list_pop_front(&(data+memnode)->sched_list);
	data[memnode].nb_task_in_queue -- ;
      }
      starpu_pthread_mutex_unlock(&(data+memnode)->policy_mutex);
      
      return task ;
    }
  }
}

struct starpu_task* pop_task_limited_choice_no_alloc(unsigned sched_ctx_id) {
  return _pop_task_limited_choice_no_alloc(sched_ctx_id, NUMBER_LOOKED_TASKS); 
}


struct starpu_task *pop_task_choice_no_alloc(unsigned sched_ctx_id){
  return _pop_task_limited_choice_no_alloc(sched_ctx_id, 1000000); 
}
