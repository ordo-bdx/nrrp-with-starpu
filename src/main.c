#include <starpu.h>
#include <starpu_scheduler.h>
#include <math.h>
#ifdef PARALLEL_TASKS
#include <omp.h>
#endif

#include <time.h>
#include "blas.h"

#ifndef NO_GPU
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas.h>
#include <starpu_cublas_v2.h>
#endif

#include "parse.h"
#include "Rounding/repart.h"
#include "scheduling.h"
#include "codelet.h"

#define GET2(A,i,j,N) A[(i)+(N)*(j)]
#define GET3(A,i,j,k,N) A[(i)+(N)*(j)+(N)*(N)*(k)]

#define ALG_CB 0
#define ALG_RRP 1
#define ALG_SNRRP 2
#define ALG_NRRP 3
#define ALG_HILBERT2D 4
#define ALG_SCR 5
#define ALG_NRRP3D 6
#define ALG_HILBERT3D 7

#define BRUTAL_ROUNDING 0
#define CLEVER_ROUNDING 1

#define SCHED_STAT 0
#define SCHED_STAT_RANDOM 1 
#define SCHED_STAT_CHOICE 2 
#define SCHED_STAT_CHEAPER 3
#define SCHED_STAT_LOAD 4
#define SCHED_DYN 5
#define SCHED_DYN_CHOICE 6
#define SCHED_DYN_CHEAPER 7 
#define SCHED_DYN_DMDA 8 


static struct starpu_sched_policy NRRP_sched_policy =
{
	.init_sched = init_sched,
	.add_workers = add_workers,
	.remove_workers = NULL,
	.deinit_sched = deinit_sched,
	.push_task = push_task,
	.pop_task = pop_task_no_steal,
	.post_exec_hook = NULL,
	.pop_every_task = NULL,
	.policy_name = "NRRP",
	.policy_description = "NRRP scheduling strategy"
};

static double convert_time(double val) {
  return (val/1000000); 
}

static double get_total_transfer()
{
  int long long sum_transferred = 0;

  int busid;
  int bus_cnt = starpu_bus_get_count();
  for (busid = 0; busid < bus_cnt; busid++)
    {

      struct starpu_profiling_bus_info bus_info;
      starpu_bus_get_profiling_info(busid, &bus_info);

      int long long transferred = bus_info.transferred_bytes;
      
      sum_transferred += transferred;
    }

  double d = (((((double) sum_transferred) / 1024) / 1024) / 1024);
  
  return d; 
}


int main(int argc, char *argv[])
{
  N = 9600 ;
  l = 960 ;
  int STEAL = 0 ;
  int REDUX = 0 ;
  int ALGORITHM = ALG_NRRP ;
  int ROUNDING = 0 ;
  int nb_iters = 1; 
  ParseArgs(argv,argc,&N,&l,&STEAL,&REDUX,&ALGORITHM,&ROUNDING, &nb_iters) ;
  if(N%l !=0){
    printf("Blocs non entiers\n") ;
    exit(EXIT_FAILURE) ;
  }
  srand(time(NULL)) ;  
  double **A_block,**B_block,**C_block ; double ***C_block_redux ; 
  int *nb_allocated_output ;
  int i,j,k ;
  A_block = malloc((N/l)*(N/l)*sizeof(double*)) ;
  B_block = malloc((N/l)*(N/l)*sizeof(double*)) ;
  C_block = malloc((N/l)*(N/l)*sizeof(double*)) ;
  if (A_block == NULL){
    printf("erreur allocation A \n") ;
    exit(EXIT_FAILURE) ;
  }
  if (B_block == NULL){
    printf("erreur allocation B \n") ;
    exit(EXIT_FAILURE) ;
  }
  if (C_block == NULL){
    printf("erreur allocation C \n") ;
    exit(EXIT_FAILURE) ;
  }
  if(REDUX){
    C_block_redux = malloc((N/l)*(N/l)*sizeof(double**)) ;
    nb_allocated_output = malloc((N/l)*(N/l)*sizeof(int)) ;
  }
  for(i=0;i<N/l;i++){
    for(j=0;j<N/l;j++){
      GET2(A_block,i,j,N/l) = malloc(l*l*sizeof(double)) ;
      if (GET2(A_block,i,j,N/l) == NULL){
	printf("erreur allocation A \n") ;
	exit(EXIT_FAILURE) ;
      }
      GET2(B_block,i,j,N/l) = malloc(l*l*sizeof(double)) ;
      if (GET2(B_block,i,j,N/l) == NULL){
	printf("erreur allocation B \n") ;
	exit(EXIT_FAILURE) ;
      }
      GET2(C_block,i,j,N/l) = malloc(l*l*sizeof(double)) ;
      if (GET2(C_block,i,j,N/l) == NULL){
	printf("erreur allocation C \n") ;
	exit(EXIT_FAILURE) ;
      }
    } ;
  } ;
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      GET2(GET2(A_block,i/l,j/l,N/l),i%l,j%l,l) = i+j ; 
      GET2(GET2(B_block,i/l,j/l,N/l),i%l,j%l,l) = 2*(i==j) ; 
      GET2(GET2(C_block,i/l,j/l,N/l),i%l,j%l,l) = 0 ; 
    } ;
  } ; 
  int ret;
  struct starpu_conf conf;
  //  printf("StarPU versions: %d %d %d\n", STARPU_MAJOR_VERSION, STARPU_MINOR_VERSION, STARPU_RELEASE_VERSION); 
  char* strategyName = "Stat"; 
  char* algName = "NA"; 
  char* roundingName = "NA"; 
  char* reduxName = "NoRedux";
  int choiceValue = -1; 
  starpu_conf_init(&conf);
  conf.sched_policy = &NRRP_sched_policy ;
  if(STEAL == SCHED_STAT_RANDOM){
    conf.sched_policy->pop_task = pop_task_rand_steal ;
    strategyName = "SRand";
  }
  if(STEAL == SCHED_STAT_CHOICE){
    conf.sched_policy->pop_task = pop_task_rand_choice_steal ;
    strategyName = "SChoice"; 
  }
  if(STEAL == SCHED_STAT_CHEAPER){
    conf.sched_policy->pop_task = pop_task_cheaper_steal ;
    strategyName = "SCheaper";
  }
  if(STEAL == SCHED_STAT_LOAD){
    conf.sched_policy->pop_task = pop_task_loaded_steal ;
    strategyName = "SLoad"; 
  }
  if(STEAL == SCHED_DYN){
    conf.sched_policy->pop_task = pop_task_eager_no_alloc ;
    strategyName = "Dyn"; 
  }
  if(STEAL == SCHED_DYN_CHOICE){
    conf.sched_policy->pop_task = pop_task_limited_choice_no_alloc ;
    strategyName = "DChoice"; 
  }
  if(STEAL == SCHED_DYN_CHEAPER){
    conf.sched_policy->pop_task = pop_task_choice_no_alloc ;
    strategyName = "DCheaper"; 
  }

  if(STEAL == SCHED_DYN_DMDA) {
    conf.sched_policy = NULL; 
    conf.sched_policy_name = "dmda"; 
    strategyName = "DMDA"; 
  }

  if(STEAL < 5) {
    switch(ALGORITHM) {
    case ALG_CB: 
      algName = "ColumnBased"; break; 
    case ALG_RRP: 
      algName = "RRP"; break; 
    case ALG_SNRRP: 
      algName = "SNRRP"; break; 
    case ALG_NRRP: 
      algName = "NRRP"; break; 
    case ALG_HILBERT2D: 
      algName = "Hilbert2D"; break; 
    case ALG_SCR: 
      algName = "SCR"; break; 
    case ALG_NRRP3D: 
      algName = "NRRP3D"; break; 
    case ALG_HILBERT3D: 
      algName = "Hilbert3D"; break; 
    }
    if(ROUNDING == BRUTAL_ROUNDING) {
      roundingName = "Coarse";
    } else {
      roundingName = "Precise"; 
    }
  }

  if(REDUX)
    reduxName = "Redux"; 


  conf.calibrate = 1 ;
  ret = starpu_init(&conf);
  if (ret == -ENODEV)
    return 77;
  STARPU_CHECK_RETURN_VALUE(ret, "starpu_init");

#ifndef NO_GPU
  starpu_cublas_init(); 
#endif
  if(thomas_scheduling_initialized) {
    if(STEAL == SCHED_STAT_CHOICE || STEAL == SCHED_DYN_CHOICE) {
      choiceValue = NUMBER_LOOKED_TASKS; 
    }
  }
  
  
#ifdef PARALLEL_TASKS
  struct starpu_cluster_machine* clusters; 
  clusters = starpu_cluster_machine(HWLOC_OBJ_SOCKET, 0); 
  //  starpu_cluster_print(clusters); 
  const int default_sched_ctx = clusters->id; 
#else 
  const int default_sched_ctx = 0; 
#endif


  if(STEAL >= 5){
    struct sched_data_struct *data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(default_sched_ctx);
    data_struct->no_alloc = 1 ;
  }
  
  int nb_proc = starpu_worker_get_count() ;

  starpu_data_handle_t *A_handle ; 
  A_handle = malloc((N/l)*(N/l)*sizeof(starpu_data_handle_t)) ;
  for(i=0;i<N/l;i++){
    for (j =0;j<N/l;j++){
      starpu_matrix_data_register(&GET2(A_handle,i,j,N/l),STARPU_MAIN_RAM, (uintptr_t)(GET2(A_block,i,j,N/l)),l,l,l,sizeof(double)) ;
    } ;
  } ;  
  starpu_data_handle_t *B_handle ; 
  B_handle = malloc((N/l)*(N/l)*sizeof(starpu_data_handle_t)) ;
  for(i=0;i<N/l;i++){
    for (j =0;j<N/l;j++){
      starpu_matrix_data_register(&GET2(B_handle,i,j,N/l),STARPU_MAIN_RAM, (uintptr_t)(GET2(B_block,i,j,N/l)),l,l,l,sizeof(double)) ;
    } ;
  } ;  
  starpu_data_handle_t *C_handle ; 
  C_handle = malloc((N/l)*(N/l)*sizeof(starpu_data_handle_t)) ;
  for(i=0;i<N/l;i++){   
    for (j =0;j<N/l;j++){
      starpu_matrix_data_register(&GET2(C_handle,i,j,N/l),STARPU_MAIN_RAM, (uintptr_t)(GET2(C_block,i,j,N/l)) ,l ,l,l,sizeof(double)) ;
    } ;
  } ;
  starpu_data_handle_t **C_aux_handle ;
  C_aux_handle = malloc((N/l)*(N/l)*sizeof(starpu_data_handle_t*)) ;

  double alloc_time = starpu_timing_now() ;
  int *repart = malloc((N/l)*(N/l)*(N/l)*sizeof(int)) ;
  if(repart == NULL){
    printf("erreur allocation repart\n") ;
    exit(EXIT_FAILURE) ;
  }
  int * task_counts = NULL; 

  int **is_allocated ;
  if(REDUX){
    is_allocated = malloc((N/l)*(N/l)*sizeof(int*)) ;
  }
  int nb_memnode = 0 ;
  if(STEAL < 5){
    struct starpu_perfmodel pf  = {.type = STARPU_PERFMODEL_INVALID };
    struct starpu_task *task_temp = starpu_task_create();
    task_temp->handles[0] = GET2(A_handle,0,0,N/l) ;
    task_temp->handles[1] = GET2(B_handle,0,0,N/l) ;
    task_temp->handles[2] = GET2(C_handle,0,0,N/l) ;
    task_temp->cl = &cl ;

    double *speed ;
    int *id_memnode ;
    id_memnode = malloc(STARPU_NMAXWORKERS*sizeof(int)) ;
    speed = malloc(STARPU_NMAXWORKERS*sizeof(double)) ;
    int nb_procs[STARPU_NMAXWORKERS]; 
    int all_perfmodels_valid = 1; 
    for(i=0;i<nb_proc;i++){
      speed[i] = 0 ; nb_procs[i] = 0; 
    }
    for(i = 0; i < nb_proc; i++){
#ifdef PARALLEL_TASKS
      if(starpu_worker_is_blocked_in_parallel(i))
	continue; 
#endif
      int memnode = starpu_worker_get_memory_node(i) ;
#ifdef NO_CPU
      if(starpu_worker_get_type(i) != STARPU_CPU_WORKER){
	if(speed[memnode] == 0){
	  id_memnode[nb_memnode] = memnode ;
	  nb_memnode ++ ;
	}
	nb_procs[memnode] ++; 
	
	double aux = starpu_task_expected_length( task_temp, starpu_worker_get_perf_archtype(i, default_sched_ctx), 0); 
	//printf("%lf\n",aux) ;
	
	if(isnan(aux) || aux == 0){
	  printf("Warning: perf model for %d (memnode %d) is invalid, setting all speeds to 1. -- sched_ctx = %d, aux=%g \n", i, memnode, default_sched_ctx, aux);
	  all_perfmodels_valid = 0; 
	  speed[memnode] = -1; 
	  // No break; here because we still want to count the number of memory nodes.
	}
	else{
	speed[memnode] = speed[memnode] + 1/aux;
	}
      }
#else
      if(speed[memnode] == 0){
	id_memnode[nb_memnode] = memnode ;
	nb_memnode ++ ;
      }
      nb_procs[memnode] ++; 

      double aux = starpu_task_expected_length( task_temp, starpu_worker_get_perf_archtype(i, default_sched_ctx), 0); 
      //printf("%lf\n",aux) ;
	
      if(isnan(aux) || aux == 0){
	printf("Warning: perf model for %d (memnode %d) is invalid, setting all speeds to 1. -- sched_ctx = %d, aux=%g \n", i, memnode, default_sched_ctx, aux);
	all_perfmodels_valid = 0; 
	speed[memnode] = -1; 
	// No break; here because we still want to count the number of memory nodes.
      }
      else{
	speed[memnode] = speed[memnode] + 1/aux;
      }
#endif
    }

    if(!all_perfmodels_valid) {
      for(i = 0; i < nb_memnode; i++) {
	speed[i] = 1; 
      }
    }
    
    task_temp->detach = 0 ;
    starpu_task_destroy(task_temp) ;
    starpu_perfmodel_unload_model(&pf);
  
    double * surfaces = malloc(nb_memnode*sizeof(double)) ;
    double speed_tot = 0 ;
    for(i=0;i<nb_memnode;i++){
      speed_tot = speed_tot + speed[id_memnode[i]] ;
    }
    for(i=0;i<nb_memnode;i++){
      surfaces[i] = speed[id_memnode[i]]/speed_tot ;
      //printf("memnode %d : %lf\n",id_memnode[i],surfaces[i]) ;
    }
    //surfaces[0] = 1. ;
    //surfaces[1] = 3./4 ;
    free(speed) ;
    RepartFinal(surfaces,nb_memnode,repart,N/l,ALGORITHM,ROUNDING) ;
    int* counts = malloc(nb_memnode * sizeof(int)); 
    task_counts = malloc(nb_memnode* sizeof(int)); 
    CheckRep(surfaces, nb_memnode, repart, N/l, ALGORITHM, ROUNDING, counts); 
    for(i = 0; i < nb_memnode; i++) {
      task_counts[i] = counts[id_memnode[i]]; 
    }
    for(i=0;i<nb_memnode;i++){
      fprintf(stderr, "%d %d %f %d \n",id_memnode[i], nb_procs[id_memnode[i]], surfaces[i], counts[i]) ;
    }
    
    for(i=0;i<N/l;i++){
      for(j=0;j<N/l;j++){
	if(REDUX){
	  GET2(is_allocated,i,j,N/l)= malloc(STARPU_NMAXWORKERS*sizeof(int)) ;
	  for(k=0;k<STARPU_NMAXWORKERS;k++){
	    GET2(is_allocated,i,j,N/l)[k] = 0 ;
	  }
	  GET2(nb_allocated_output,i,j,N/l) = 0 ;
	}
	for(k=0;k<N/l;k++){
	  GET3(repart, i, j, k, N/l) = id_memnode[GET3(repart, i, j, k, N/l)];
	  //printf("%i %i %i %i %i\n",i,j,k,REDUX,GET3(repart,i,j,k,N/l)) ;
	  if(REDUX && !GET2(is_allocated,i,j,N/l)[GET3(repart, i, j, k, N/l)]){
	    GET2(nb_allocated_output,i,j,N/l) ++ ;
	    GET2(is_allocated,i,j,N/l)[GET3(repart, i, j, k, N/l)] = 1 ;
	  }
	}
	if(REDUX && GET2(nb_allocated_output,i,j,N/l) > 1){
	  GET2(C_block_redux,i,j,N/l) = malloc(STARPU_NMAXWORKERS*sizeof(double*)) ;	  
	  GET2(C_aux_handle,i,j,N/l)= malloc(STARPU_NMAXWORKERS*sizeof(starpu_data_handle_t)) ;
	  for(k=0;k<STARPU_NMAXWORKERS;k++){
	    if(GET2(is_allocated,i,j,N/l)[k] && k != STARPU_MAIN_RAM){
	      GET2(C_block_redux,i,j,N/l)[k] = malloc(l*l*sizeof(double)) ; 
	      int ibis,jbis ;
	      for(ibis=0;ibis < l;ibis ++){
		for(jbis = 0;jbis < l;jbis ++){
		  GET2(GET2(C_block_redux,i,j,N/l)[k],ibis,jbis,l) = 0 ;
		}
	      }
	      starpu_matrix_data_register(&(GET2(C_aux_handle,i,j,N/l)[k]),STARPU_MAIN_RAM, (uintptr_t) (GET2(C_block_redux,i,j,N/l)[k]),l,l,l,sizeof(double)) ;
	    }
	  }
	}
      }
    }
    //printf("coucou\n") ;

    free(counts); 
    free(surfaces) ;
    free(id_memnode) ;
    alloc_time = starpu_timing_now() - alloc_time ;
    
    
    //printf("fin allocation : %lf secondes\n",temps/1000000) ;
    // printf("ok %i %i %i\n",N,l,N/l);
  }

  //  starpu_profiling_status_set(STARPU_PROFILING_DISABLE); 

  // Dry run to warm up the GPUs
  struct sched_data_struct *data_struct = NULL;  int memnode ;  
  struct sched_data *data = NULL;
  if(STEAL<5 && STEAL > 0 && thomas_scheduling_initialized){
    data_struct = (struct sched_data_struct*) starpu_sched_ctx_get_policy_data(default_sched_ctx);
    data = data_struct->data ;

  }
  int iteration_number; 
  for (iteration_number = 0; iteration_number < 1; iteration_number++) {

    for(k=0;k<N/l;k++){
      for(i=0;i<N/l;i++){
	for(j=0;j<N/l;j++){
	  //printf("%i %i %i %i %i %i %i\n",i,j,k,REDUX,GET3(repart,i,j,k,N/l),GET2(nb_allocated_output,i,j,N/l),GET2(is_allocated,i,j,N/l)[GET3(repart,i,j,k,N/l)]) ;
	  int ret ;       
	  if(REDUX && GET2(nb_allocated_output,i,j,N/l) > 1 && 
	     GET3(repart, i, j, k, N/l) != STARPU_MAIN_RAM){     
	    ret = starpu_task_insert(&cl,
				     STARPU_VALUE,&i,sizeof(i),
				     STARPU_VALUE,&j,sizeof(j),
				     STARPU_VALUE,&k,sizeof(k),
				     STARPU_VALUE,&GET3(repart,i,j,k,N/l),sizeof(int),
				     STARPU_R, GET2(A_handle,i,k,N/l),
				     STARPU_R, GET2(B_handle,k,j,N/l),
				     STARPU_RW, GET2(C_aux_handle,i,j,N/l)[GET3(repart,i,j,k,N/l)],0) ;
	    //printf("insert\n") ;
	  }
	  else{
	    ret = starpu_task_insert(&cl,
				     STARPU_VALUE,&i,sizeof(i),
				     STARPU_VALUE,&j,sizeof(j),
				     STARPU_VALUE,&k,sizeof(k),
				     STARPU_VALUE,&GET3(repart,i,j,k,N/l),sizeof(int),
				     STARPU_R, GET2(A_handle,i,k,N/l),
				     STARPU_R, GET2(B_handle,k,j,N/l),
				     STARPU_RW, GET2(C_handle,i,j,N/l),0) ;
	    //printf("insert\n") ;
	  }
	  STARPU_CHECK_RETURN_VALUE(ret, "task insertion");
	} ;
      } ;
    } ;

    if(REDUX){
      for(i=0;i<N/l;i++){
	for(j=0;j<N/l;j++){
	  if(GET2(nb_allocated_output,i,j,N/l)>1){
	    for(k=0;k<STARPU_NMAXWORKERS;k++){
	      if(GET2(is_allocated,i,j,N/l)[k] && k != STARPU_MAIN_RAM){
		int ret =  starpu_task_insert(&redux_codelet,
					      STARPU_RW, GET2(C_handle,i,j,N/l),
					      STARPU_R, GET2(C_aux_handle,i,j,N/l)[k],0) ;
		STARPU_CHECK_RETURN_VALUE(ret, "task insertion");
	      }
	   }
	  }
	}
      }
    }


    if(data){
      for(memnode = 0;memnode<nb_memnode;memnode++){
	data[memnode].has_began = 1 ;
      }
    }
    
    starpu_task_wait_for_all();
    
    if(data){
      for(memnode = 0;memnode<nb_memnode;memnode++){
	data[memnode].has_began = 0 ;
      }
    }
    
    for(i=0;i<N/l;i++){
      int j ;
      for(j=0;j<N/l;j++){
	starpu_data_unregister(GET2(A_handle,i,j,N/l)) ;
	starpu_data_unregister(GET2(B_handle,i,j,N/l)) ;
	starpu_data_unregister(GET2(C_handle,i,j,N/l)) ;
	if(REDUX && GET2(nb_allocated_output,i,j,N/l)>1){
	  for(k=0;k<STARPU_NMAXWORKERS;k++){
	    if(GET2(is_allocated,i,j,N/l)[k] && k != STARPU_MAIN_RAM){
	      starpu_data_unregister(GET2(C_aux_handle,i,j,N/l)[k]) ;
	    }
	  }
	}
      } ;
    } ;
    starpu_task_wait_for_all(); 
    
  }

 /* for(i=0;i<N;i++){ */
 /*    for(j=0;j<N;j++){ */
 /*      //printf("%i ",(int) GET2(GET2(C_block,i/l,j/l,N/l),i%l,j%l,l)) ; */
 /*      if((int) GET2(GET2(C_block,i/l,j/l,N/l),i%l,j%l,l) != 2*(i+j)){  */
 /* 	  	printf("wrong !\n") ; */
 /* 	exit(1);  */
 /*      } */
 /*    } ; */
 /*    //printf("\n") ; */
 /*  } ; */

  // Real run, now.
  

  
  for(iteration_number = 0; iteration_number < nb_iters; iteration_number++) {

  starpu_profiling_status_set(STARPU_PROFILING_ENABLE); 

  for(i=0;i<N/l;i++){
    for (j =0;j<N/l;j++){
      starpu_matrix_data_register(&GET2(A_handle,i,j,N/l),STARPU_MAIN_RAM, (uintptr_t)(GET2(A_block,i,j,N/l)),l,l,l,sizeof(double)) ;
      starpu_matrix_data_register(&GET2(B_handle,i,j,N/l),STARPU_MAIN_RAM, (uintptr_t)(GET2(B_block,i,j,N/l)),l,l,l,sizeof(double)) ;
      starpu_matrix_data_register(&GET2(C_handle,i,j,N/l),STARPU_MAIN_RAM, (uintptr_t)(GET2(C_block,i,j,N/l)),l,l,l,sizeof(double)) ;
      if(REDUX && GET2(nb_allocated_output,i,j,N/l)>1){
	for(k=0;k<STARPU_NMAXWORKERS;k++){
	  if(GET2(is_allocated,i,j,N/l)[k] && k != STARPU_MAIN_RAM){
	    int ibis,jbis ;
	    for(ibis=0;ibis < l;ibis ++){
	      for(jbis = 0;jbis < l;jbis ++){
		GET2(GET2(C_block_redux,i,j,N/l)[k],ibis,jbis,l) = 0 ;
	      }
	    }
	    starpu_matrix_data_register(&(GET2(C_aux_handle,i,j,N/l)[k]),STARPU_MAIN_RAM, (uintptr_t)(GET2(C_block_redux,i,j,N/l)[k]),l,l,l,sizeof(double)) ;
	  }
	}
	
      } ;
    } ;  
  }

  double temps_calcul = starpu_timing_now() ;
  for(k=0;k<N/l;k++){
  for(i=0;i<N/l;i++){
    for(j=0;j<N/l;j++){
      int ret ;       
	  if(REDUX && GET2(nb_allocated_output,i,j,N/l) > 1 && 
	     GET3(repart, i, j, k, N/l) != STARPU_MAIN_RAM){     
	    ret = starpu_task_insert(&cl,
				     STARPU_VALUE,&i,sizeof(i),
				     STARPU_VALUE,&j,sizeof(j),
				     STARPU_VALUE,&k,sizeof(k),
				     STARPU_VALUE,&GET3(repart,i,j,k,N/l),sizeof(int),
				     STARPU_R, GET2(A_handle,i,k,N/l),
				     STARPU_R, GET2(B_handle,k,j,N/l),
				     STARPU_RW, GET2(C_aux_handle,i,j,N/l)[GET3(repart,i,j,k,N/l)],0) ;
	    //printf("insert\n") ;
	  }
	  else{
	    ret = starpu_task_insert(&cl,
				     STARPU_VALUE,&i,sizeof(i),
				     STARPU_VALUE,&j,sizeof(j),
				     STARPU_VALUE,&k,sizeof(k),
				     STARPU_VALUE,&GET3(repart,i,j,k,N/l),sizeof(int),
				     STARPU_R, GET2(A_handle,i,k,N/l),
				     STARPU_R, GET2(B_handle,k,j,N/l),
				     STARPU_RW, GET2(C_handle,i,j,N/l),0) ;
	    //printf("insert\n") ;
	  }
	STARPU_CHECK_RETURN_VALUE(ret, "task insertion");
      } ;
    } ;
  } ;

  if(REDUX){
    for(i=0;i<N/l;i++){
      for(j=0;j<N/l;j++){
	if(GET2(nb_allocated_output,i,j,N/l)>1){
	  for(k=0;k<STARPU_NMAXWORKERS;k++){
	    if(GET2(is_allocated,i,j,N/l)[k] && k != STARPU_MAIN_RAM){
	      int ret =  starpu_task_insert(&redux_codelet,
					    STARPU_RW, GET2(C_handle,i,j,N/l),
					    STARPU_R, GET2(C_aux_handle,i,j,N/l)[k],0) ;
	      STARPU_CHECK_RETURN_VALUE(ret, "task insertion");
	    }
	  }
	  }
      }
    }
  }


  if(data){
    for(memnode = 0;memnode<nb_memnode;memnode++){
      data[memnode].has_began = 1 ;
    }
  }
  starpu_task_wait_for_all();
  if(data){
    for(memnode = 0;memnode<nb_memnode;memnode++){
      data[memnode].has_began = 0 ;
    }
  }

    
    starpu_task_wait_for_all();
  temps_calcul = starpu_timing_now() - temps_calcul ; 
  starpu_profiling_status_set(STARPU_PROFILING_DISABLE); 

  for(i=0;i<N/l;i++){
    int j ;
    for(j=0;j<N/l;j++){
      starpu_data_unregister(GET2(A_handle,i,j,N/l)) ;
      starpu_data_unregister(GET2(B_handle,i,j,N/l)) ;
      starpu_data_unregister(GET2(C_handle,i,j,N/l)) ;
      if(REDUX && GET2(nb_allocated_output,i,j,N/l)>1){
	for(k=0;k<STARPU_NMAXWORKERS;k++){
	  if(GET2(is_allocated,i,j,N/l)[k] && k != STARPU_MAIN_RAM){
	    starpu_data_unregister(GET2(C_aux_handle,i,j,N/l)[k]) ;
	  }
	}
      }
    } ;
  } ;

  starpu_task_wait_for_all();
  
  double comms = get_total_transfer(); 
  printf("%d %d %s %d %s %s %s %d %lf %lf %lf\n", N, l, strategyName, choiceValue, algName, roundingName, reduxName, iteration_number, 
	 convert_time(alloc_time), convert_time(temps_calcul), comms); 
  
  }

/* terminate StarPU */
  //printf("before end\n") ;
#ifdef PARALLEL_TASKS
  starpu_uncluster_machine(clusters); 
#endif
  starpu_cublas_shutdown(); 
  starpu_shutdown();


 /* for(i=0;i<N;i++){ */
 /*    for(j=0;j<N;j++){ */
 /*      //printf("%i ",(int) GET2(GET2(C_block,i/l,j/l,N/l),i%l,j%l,l)) ; */
 /*      if((int) GET2(GET2(C_block,i/l,j/l,N/l),i%l,j%l,l) != N*N){ */
 /* 	printf("wrong !\n") ; */
 /*      } */
 /*    } ; */
 /*    //printf("\n") ; */
 /*  } ; */
 for(i= 0;i<N/l;i++){
   for(j=0;j<N/l;j++){
      free(GET2(A_block,i,j,N/l)) ;
      free(GET2(B_block,i,j,N/l)) ;
      free(GET2(C_block,i,j,N/l)) ;
   }
 }
 free(repart) ;
 free(A_block) ;
 free(B_block) ;
 free(C_block) ;
 free(A_handle) ;
 free(B_handle) ;
 free(C_handle) ;

 // print_time("AllocTime", STEAL < 5? alloc_time :0); 
 //print_time("ComputeTime", temps_total); 
 return 0;
}
