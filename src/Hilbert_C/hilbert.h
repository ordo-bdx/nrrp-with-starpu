#ifndef HILBERT
#define HILBERT

void Hilbert2D(double *surfaces,int P,int *repart,int log_N) ; //implementation of SFCP, given the respectives surfaces (which sum must be one), the number of processor P, and the binary log of the size of the matrix, log_N, fill the matrix repart with the allocated processors.

void Hilbert3D(double *volumes,int P,int *repart,int log_N) ; //implementation of 3D-SFCP, given the respectives volumes (which sum must be one), the number of processor P, and the binary log of the size of the matrix, log_N, fill the three-dimensional array repart with the allocated processors.

#endif
