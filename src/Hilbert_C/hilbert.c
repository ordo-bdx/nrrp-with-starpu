//+++++++++++++++++++++++++++ PUBLIC-DOMAIN SOFTWARE ++++++++++++++++++++++++++
// Functions: TransposetoAxes  AxestoTranspose
// Purpose:   Transform in-place between Hilbert transpose and geometrical axes
// Example:   b=5 bits for each of n=3 coordinates.
//            15-bit Hilbert integer = A B C D E F G H I J K L M N O  is stored
//            as its Transpose
//                   X[0] = A D G J M                X[2]|
//                   X[1] = B E H K N    <------->       | /X[1]
//                   X[2] = C F I L O               axes |/
//                          high  low                    0------ X[0]
//            Axes are stored conventially as b-bit integers.
// Author:    John Skilling  20 Apr 2001 to 11 Oct 2003
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


#define GET2(A,i,j,N) A[(i)+(N)*(j)]
#define GET3(A,i,j,k,N) A[(i)+(N)*(j)+(N)*(N)*(k)]

typedef unsigned int coord_t; // char,short,int for up to 8,16,32 bits per word

void TransposetoAxes( coord_t* X, int b, int n )  // position, #bits, dimension
{    
  coord_t  N = 2 << (b-1), P, Q, t;
  int  i;
  // Gray decode by H ^ (H/2)
  t = X[n-1] >> 1;
  for( i = n-1; i > 0; i-- ) X[i] ^= X[i-1];
  X[0] ^= t;
  // Undo excess work
  for( Q = 2; Q != N; Q <<= 1 ) {
    P=Q-1;
    for( i = n-1; i >= 0 ; i-- )
      if( X[i] & Q ) X[0] ^= P;                              // invert
      else{ t = (X[0]^X[i]) & P; X[0] ^= t; X[i] ^= t; }  }  // exchange
}

void AxestoTranspose( coord_t* X, int b, int n )  // position, #bits, dimension
{    
  coord_t  M = 1 << (b-1), P, Q, t;
  int  i;
  // Inverse undo
  for( Q = M; Q > 1; Q >>= 1 ) {
    P=Q-1;
    for( i = 0; i < n; i++ )
      if( X[i] & Q ) X[0] ^= P;                              // invert
      else{ t = (X[0]^X[i]) & P; X[0] ^= t; X[i] ^= t; }  }  // exchange
  // Gray encode
  for( i = 1; i < n; i++ ) 
    X[i] ^= X[i-1];
  t=0;
  for( Q = M; Q > 1; Q >>= 1 )
    if( X[n-1] & Q ) t ^= Q-1;
  for( i = 0; i < n; i++ ) X[i] ^= t;
}

int ConverttoPos(coord_t *X,int b, int n){
  int i,j ;
  int res = 0 ;
  for(i=b-1;i>=0;i--){
    for(j=0;j<n;j++){
      res = res * 2 + (X[j]>>i & 1) ;
    }
  }
  return res ;
}

int HilbertPos(coord_t *X,int b, int n){
  AxestoTranspose(X, b, n);  
  return ConverttoPos(X,b,n);
}

int Round(double d){
  int n = floor(d);
  if (d-n >= 0.5){
      n++ ;
    }
  return n ;
}

void Hilbert2D(double *volumes,int P,int *repart,int log_N){
  int i,j ;
  int N = pow(2,log_N) ;
  double *last_task ;
  last_task = malloc(P*sizeof(double)) ;
  last_task[0] = pow(N,2)*volumes[0] ;
  for(i=1;i<P;i++){
    last_task[i] = pow(N,2)*volumes[i]+last_task[i-1] ;
    last_task[i-1] = Round(last_task[i-1]) -1 ;
  }
  last_task[P-1] = Round(last_task[P-1]) - 1 ;
  coord_t X[2] ;
  int pos,proc ;
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
	X[0] = i ;X[1] = j ;
	pos = HilbertPos(X,log_N,2) ;
	proc = 0 ;
	while(last_task[proc]<pos){
	  proc ++ ;
	}
	GET2(repart,i,j,N) = proc ;
    }
  }
  free(last_task) ;
}

void Hilbert3D(double *volumes,int P,int *repart,int log_N){
  int i,j,k ;
  int N = pow(2,log_N) ;
  double *last_task ;
  last_task = malloc(P*sizeof(double)) ;
  last_task[0] = pow(N,3)*volumes[0] ;
  for(i=1;i<P;i++){
    last_task[i] = pow(N,3)*volumes[i]+last_task[i-1] ;
    last_task[i-1] = Round(last_task[i-1]) -1 ;
  }
  last_task[P-1] = Round(last_task[P-1]) - 1 ;
  coord_t X[3] ;
  int pos,proc ;
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      for(k=0;k<N;k++){
	X[0] = i ;X[1] = j ; X[2] = k ;
	pos = HilbertPos(X,log_N,3) ;
	proc = 0 ;
	while(last_task[proc]<pos){
	  proc ++ ;
	}
	GET3(repart,i,j,k,N) = proc ;
      }
    }
  }
  free(last_task) ;
}
