#ifndef SCHEDULING
#define SCHEDULING

#include <starpu.h>
#include <starpu_scheduler.h>

struct sched_data
{
  struct starpu_task_list sched_list; 
  starpu_pthread_mutex_t policy_mutex; 
  int has_began ;
  int is_init ;
  int nb_task_in_queue; 
};

struct sched_data_struct
{
  struct sched_data * data ;
  int nb_memnode ;
  int *id_memnode ;
  int no_alloc ;
  int nb_no_alloc_task_in_queue ;
  starpu_pthread_mutex_t no_alloc_mutex ;
  struct starpu_task_list sched_list_no_alloc;
};

int thomas_scheduling_initialized; 
int NUMBER_LOOKED_TASKS; 

void init_sched(unsigned sched_ctx_id) ;

void add_workers(unsigned sched_ctx_id, int *workerids, unsigned nworkers) ;

void deinit_sched(unsigned sched_ctx_id) ; 

int push_task(struct starpu_task *task) ;

struct starpu_task *pop_task_no_steal(unsigned sched_ctx_id) ;

struct starpu_task *pop_task_rand_steal(unsigned sched_ctx_id) ;

struct starpu_task *pop_task_loaded_steal(unsigned sched_ctx_id) ;

struct starpu_task *pop_task_rand_choice_steal(unsigned sched_ctx_id) ;

struct starpu_task *pop_task_cheaper_steal(unsigned sched_ctx_id) ;

struct starpu_task *pop_task_eager_no_alloc(unsigned sched_ctx_id) ;

struct starpu_task *pop_task_limited_choice_no_alloc(unsigned sched_ctx_id) ;

struct starpu_task *pop_task_choice_no_alloc(unsigned sched_ctx_id) ;

#endif
