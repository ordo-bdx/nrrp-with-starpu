#!/bin/bash

exec > results.all

nbIters=25
exec="../test -i $nbIters"

allocAlgs="-NRRP"
allocAlgs3D="-NRRP3D"
reduxOption="-Redux"
stealStrats="-Stat -SRand -SChoice -SCheaper"
dynStrats="-Dyn -DChoice -DCheaper -Ddmda"
NValues="7680 15360 23040 30720"
choiceValues="10 50"
rounding="-SmartRounding -NoRounding"

function do_run_choice() {
    for c in $choiceValues; do 
	NUMBER_LOOKED_TASKS=$c  $exec $a $s -N $N $r $o 2> /dev/null
    done
    c=
}

function do_run_simple() {
	$exec $s $a -N $N $r $o 2>/dev/null
  
}

function do_run () {
    if echo $s | grep -q "DChoice"; then 
	do_run_choice
    else
	do_run_simple
    fi
}


for N in $NValues; do 
    a=
    for s in $dynStrats; do 
	o=
	do_run
    done
    for s in $stealStrats; do 
	for r in $rounding; do 
	    for a in $allocAlgs; do
		o=
		do_run
	    done
	    for a in $allocAlgs3D; do 
		o=
		do_run
		o=$reduxOption
		do_run
	    done
	done
    done
done


