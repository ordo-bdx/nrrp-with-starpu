#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ALG_CB 0
#define ALG_RRP 1
#define ALG_SNRRP 2
#define ALG_NRRP 3
#define ALG_HILBERT2D 4
#define ALG_SCR 5
#define ALG_NRRP3D 6
#define ALG_HILBERT3D 7

#define BRUTAL_ROUNDING 0
#define CLEVER_ROUNDING 1

#define SCHED_STAT 0
#define SCHED_STAT_RANDOM 1 
#define SCHED_STAT_CHOICE 2 
#define SCHED_STAT_CHEAPER 3
#define SCHED_STAT_LOAD 4
#define SCHED_DYN 5
#define SCHED_DYN_CHOICE 6
#define SCHED_DYN_CHEAPER 7 
#define SCHED_DYN_DMDA 8

void ParseOneArg(char *arg[],int *i,int *N,int *l,int *steal, int *redux, int *algorithm, int *rounding, int* iters){
  if(strcmp(arg[*i],"-N")==0){
    *N = atoi(arg[*i+1]) ;
    *i = *i+1 ;
    return ;
  }
  if(strcmp(arg[*i],"-l")==0){
    *l = atoi(arg[*i+1]) ;
    *i = *i+1 ;
    return ;
  }
  if(strcmp(arg[*i],"-i")==0){
    *iters = atoi(arg[*i+1]) ;
    *i = *i+1 ;
    return ;
  }
  if(strcmp(arg[*i],"-Redux")==0){
    *redux = 1 ;
    return ;
  }
  if(strcmp(arg[*i],"-SmartRounding")==0){
    *rounding = CLEVER_ROUNDING ;
    return ;
  }
  if(strcmp(arg[*i],"-NoRounding")==0){
    *rounding = BRUTAL_ROUNDING ;
    return ;
  }
  if(strcmp(arg[*i],"-NRRP")==0){
    *algorithm = ALG_NRRP ;
    return ;
  }
  if(strcmp(arg[*i],"-RRP")==0){
    *algorithm = ALG_RRP ;
    return ;
  }
  if(strcmp(arg[*i],"-SNRRP")==0){
    *algorithm = ALG_SNRRP ;
    return ;
  }
  if(strcmp(arg[*i],"-CB")==0){
    *algorithm = ALG_CB ;
    return ;
  }
  if(strcmp(arg[*i],"-NRRP3D")==0){
    *algorithm = ALG_NRRP3D ;
    return ;
  }
  if(strcmp(arg[*i],"-H2D")==0){
    *algorithm = ALG_HILBERT2D ;
    return ;
  }
  if(strcmp(arg[*i],"-SCR")==0){
    *algorithm = ALG_SCR   ;
    return ;
  }
  if(strcmp(arg[*i],"-H3D")==0){
    *algorithm = ALG_HILBERT3D ;
    return ;
  }
  if(strcmp(arg[*i],"-Stat")==0){
    *steal = SCHED_STAT ;
    return ;
  }
  if(strcmp(arg[*i],"-SRand")==0){
    *steal = SCHED_STAT_RANDOM ;
    return ;
  }
  if(strcmp(arg[*i],"-SChoice")==0){
    *steal = SCHED_STAT_CHOICE ;
    return ;
  }
  if(strcmp(arg[*i],"-SCheaper")==0){
    *steal = SCHED_STAT_CHEAPER ;
    return ;
  }
  if(strcmp(arg[*i],"-SLoad")==0){
    *steal = SCHED_STAT_LOAD ;
    return ;
  }
  if(strcmp(arg[*i],"-Dyn")==0){
    *steal = SCHED_DYN ;
    return ;
  }
  if(strcmp(arg[*i],"-DChoice")==0){
    *steal = SCHED_DYN_CHOICE ;
    return ;
  }
  if(strcmp(arg[*i],"-DCheaper")==0){
    *steal = SCHED_DYN_CHEAPER ;
    return ;
  }
  if(strcmp(arg[*i],"-Ddmda")==0){
    *steal = SCHED_DYN_DMDA ;
    return ;
  }
  if(strcmp(arg[*i],"-help")==0){
    printf("Available Options :\n") ;
    printf("   -N XX :                    Set the size of the matrix to XX (default 9600)\n") ;
    printf("   -l XX :                    Set the size of the tiles to XX (default 960)\n") ;
    printf("   -Redux :                   Use Redux mode\n") ;
    printf("   -SmartRounding :           Use Precise mode (decrease communication ratio for load balancing)\n") ;
    printf("   -NRRP :                    Use NRRP as static allocation (default)\n") ;
    printf("   -RRP :                     Use RRP as static allocation\n") ;
    printf("   -SNRRP :                   Use SNRRP as static allocation\n") ;
    printf("   -NRRP3D :                  Use NRRP3D as static allocation\n") ;
    printf("   -CB :                      Use ColumnBased as static allocation\n") ;
    printf("   -SCR :                     Use SCR (3D-ColumnBased) as static allocation\n") ;
    printf("   -H2D :                     Use SFCP as static allocation (N/l must be a power of 2)\n") ;
    printf("   -H3D :                     Use 3D-SFCP as static allocation (N/l must be a power of 2)\n") ;
    printf("   -Stat :                    Use purely static scheduler (default)\n") ;
    printf("   -SRand :                   Use static scheduler with random work-stealing\n") ;
    printf("   -SChoice :                 Use static scheduler with limited choice work-stealing\n") ;
    printf("   -SCheaper :                Use static scheduler with optimized communicaton work-stealing\n") ;
    printf("   -SLoad :                   Use static scheduler with work-stealing on the most loaded memory node\n") ;
    printf("   -Dyn :                     Use purely dynamic eager-type scheduler\n") ;
    printf("   -DChoice :                 Use purely dynamic limited choice with look on communication\n") ;
    printf("   -DCheaper :                Use purely dynamic eager-type schedulerwith optimized communication local choice\n") ;
    exit(EXIT_FAILURE) ;      
    return ;
  }
  printf("Option inconnue, please use \"-help\" for help\n") ;
  exit(EXIT_FAILURE) ;       
}

void ParseArgs(char *arg[],int argc,int *N,int *l,int *steal, int *redux, int *algorithmn, int *rounding, int* iters){
  int i ;
  for(i=1;i<argc;i++){
    ParseOneArg(arg,&i,N,l,steal,redux,algorithmn,rounding, iters) ;
  }
}
