#include <starpu.h>
#include <starpu_scheduler.h>
#include <math.h>
#include "NRRP_C/nrrp.h"
#include "scheduling.h"
#include <time.h>
#include "blas.h"
#ifndef NO_GPU
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas.h>
#include <starpu_cublas_v2.h>
#endif

#define GET2(A,i,j,N) A[(i)+(N)*(j)]
#define GET3(A,i,j,k,N) A[(i)+(N)*(j)+(N)*(N)*(k)]


int l,N ;
const double one = 1.0; 

void cpu_gemm_func(void *buffers[], void *cl_arg){
  double *A,*B,*C ;
  A = (double*)STARPU_MATRIX_GET_PTR(buffers[0]) ;
  B = (double*)STARPU_MATRIX_GET_PTR(buffers[1]) ;
  C = (double*)STARPU_MATRIX_GET_PTR(buffers[2]) ;
  //starpu_codelet_unpack_args(cl_arg,&i,&j,&k,&proc) ;
  //printf("task CPU: %i %i %i fait par worker %i\n",i,j,k,starpu_worker_get_id()) ;
  //  nb_task_CPU ++ ;
  STARPU_DGEMM("N","N",l,l,l,1,A,l,B,l,1,C,l) ;
}

#ifndef NO_GPU
void gpu_gemm_func(void *buffers[], void *cl_arg){
  double *A,*B,*C ;
  A = (double*)STARPU_MATRIX_GET_PTR(buffers[0]) ;
  B = (double*)STARPU_MATRIX_GET_PTR(buffers[1]) ;
  C = (double*)STARPU_MATRIX_GET_PTR(buffers[2]) ;
  //starpu_codelet_unpack_args(cl_arg,&i,&j,&k,&proc) ;
  //  nb_task_GPU ++ ;
  //printf("task GPU: %i %i %i fait par worker %i\n",i,j,k,starpu_worker_get_id()) ;
  cublasStatus_t status = cublasDgemm(starpu_cublas_get_local_handle(), CUBLAS_OP_N, CUBLAS_OP_N,l,l,l,&one,A,l,B,l,&one,C,l) ;
  if (status != CUBLAS_STATUS_SUCCESS)
    STARPU_CUBLAS_REPORT_ERROR(status);
}
#endif

/* void warm_up_gpu(void *descr[], void *cl_arg){
  double *A,*B;
  A = (double*)STARPU_MATRIX_GET_PTR(descr[0]) ;
  B = (double*)STARPU_MATRIX_GET_PTR(descr[1]) ;
  cublasStatus_t status = cublasDgemm(starpu_cublas_get_local_handle(), CUBLAS_OP_N,CUBLAS_OP_N,l,l,l,&one,A,l,A,l,&one,B,l) ;
  if (status != CUBLAS_STATUS_SUCCESS)
    STARPU_CUBLAS_REPORT_ERROR(status);
}

void warm_up_cpu(void *descr[], void *cl_arg){
  double *A,*B,*C ;
  A = (double*)STARPU_MATRIX_GET_PTR(descr[0]) ;
  B = (double*)STARPU_MATRIX_GET_PTR(descr[1]) ;
  STARPU_DGEMM("N","N",l,l,l,1,A,l,A,l,1,B,l) ;
  } */

void init_cpu_func(void *descr[], void *cl_arg)
{
  double *val = (double *)STARPU_VARIABLE_GET_PTR(descr[0]);
  int i,j ;
  for(i=0;i<l;i++)
    for(j=0;j<l;j++)
      GET2(val,i,j,l) = 0;
}


#ifndef NO_GPU
void init_gpu_func(void *descr[], void *cl_arg)
{
        double *val = (double *)STARPU_VARIABLE_GET_PTR(descr[0]);
	cudaMemsetAsync(val, 0, l*l*sizeof(double), starpu_cuda_get_local_stream());
}
#endif

void redux_cpu_func(void *descr[], void *cl_arg)
{
        double *a = (double *)STARPU_VARIABLE_GET_PTR(descr[0]);
	double *b = (double *)STARPU_VARIABLE_GET_PTR(descr[1]);
	int i,j ;
	for(i=0;i<l;i++)
	  for(j=0;j<l;j++)
	    GET2(a,i,j,l) =  GET2(a,i,j,l)+ GET2(b,i,j,l);
}


#ifndef NO_GPU
void redux_gpu_func(void *descr[], void *cl_arg)
{
  double *a = (double *)STARPU_VARIABLE_GET_PTR(descr[0]);
  double *b = (double *)STARPU_VARIABLE_GET_PTR(descr[1]);
  cublasStatus_t status = cublasDgeam(starpu_cublas_get_local_handle(), CUBLAS_OP_N,CUBLAS_OP_N,l,l,&one,a,l,&one,b,l,a,l) ;
  /*  if (status != CUBLAS_STATUS_SUCCESS)
      STARPU_CUBLAS_REPORT_ERROR(status); */

}
#endif


struct starpu_perfmodel gemm_perf_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "gemm_perf_model_2D"
};

struct starpu_codelet cl =
{
#ifdef NO_CPU
  .cpu_funcs = {},
#else
  .cpu_funcs = {cpu_gemm_func},
  .cpu_funcs_name = {"cpu_gemm_func"},
#endif
#ifndef NO_GPU
  .cuda_funcs = {gpu_gemm_func},
  .cuda_flags = {STARPU_CUDA_ASYNC},
#endif
  .opencl_funcs = {},
  .nbuffers = 3,
  //.modes = {STARPU_R,STARPU_R,STARPU_REDUX},
  .modes = {STARPU_R,STARPU_R,STARPU_RW},
  .name = "ThomasGEMM_2D",
  .model = &gemm_perf_model,
#ifdef PARALLEL_TASKS
  .type = STARPU_FORKJOIN,
  .max_parallelism = INT_MAX,
#endif
};

struct starpu_perfmodel gemm_perf_model_redux = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "gemm_perf_model_2D_redux"
};

struct starpu_codelet cl_redux =
{
  .cpu_funcs = {cpu_gemm_func},
#ifndef NO_GPU
  .cuda_funcs = {gpu_gemm_func},
  .cuda_flags = {STARPU_CUDA_ASYNC},
#endif
  .opencl_funcs = {},
  .nbuffers = 3,
  .modes = {STARPU_R,STARPU_R,STARPU_REDUX},
  .name = "ThomasGEMM_2D",
  .model = &gemm_perf_model_redux
};

/* struct starpu_codelet cl_warm_up =
{
  .cpu_funcs = {warm_up_cpu},
  .cuda_funcs = {warm_up_gpu},
  .opencl_funcs = {},
  .cuda_flags = {STARPU_CUDA_ASYNC},
  .nbuffers = 2,
  .name = "warm_up",
  }; */


struct starpu_codelet init_codelet =
{
        .cpu_funcs = {init_cpu_func},
#ifndef NO_GPU
        .cuda_funcs = {init_gpu_func},
	.cuda_flags = {STARPU_CUDA_ASYNC},
#endif
	.modes = {STARPU_W},
        .nbuffers = 1
};

struct starpu_codelet redux_codelet =
{
  .name = "MatrixSum", 
  .cpu_funcs = {redux_cpu_func},
#ifndef NO_GPU
	.cuda_funcs = {redux_gpu_func},
	.cuda_flags = {STARPU_CUDA_ASYNC},
#endif
	.modes = {STARPU_RW, STARPU_R},
	.nbuffers = 2
};


