#ifndef CUBOID
#define CUBOID

typedef struct{
  	double x1 ;
  	double x2 ;
  	double y1 ;
  	double y2 ;
	double z1 ;
	double z2 ;
} cuboid;

typedef struct node3D{
	cuboid head ;
	struct node3D *queue ;	
} zone3D ;

typedef struct{
  	int x1 ;
  	int x2 ;
  	int y1 ;
  	int y2 ;
	int z1 ;
	int z2 ;
} dcuboid;

typedef struct dnode3D{
	dcuboid head ;
	struct dnode3D *queue ;	
} dzone3D ;

void freeZone3D(zone3D *z) ;

void freeDZone3D(dzone3D *z) ;

#endif

