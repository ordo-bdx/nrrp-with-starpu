#ifndef SUBROUTINE
#define SUBROUTINE

#include "cuboid.h"

void GuilloRec3D(cuboid cub,double alpha, cuboid res[2]) ;

void Guillo3D(cuboid cub,double alpha, zone3D **res, int debut) ;

void Cube(cuboid cub,double alpha, zone3D **res, int debut) ;

void flatCube(cuboid cub, double alpha, zone3D **res,int debut) ;
#endif
