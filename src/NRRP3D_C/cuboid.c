#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cuboid.h"

void freeZone3D(zone3D *z){
  zone3D *zbis = z ;
  while(z != NULL){
    z = z-> queue ;
    free(zbis) ;
    zbis = z ;
  }
}

void freeDZone3D(dzone3D *z){
  dzone3D *zbis = z ;
  while(z != NULL){
    z = z-> queue ;
    free(zbis) ;
    zbis = z ;
  }
}
