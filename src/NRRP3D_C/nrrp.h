#ifndef SUBROUTINE3D
#define SUBROUTINE3D

#include "cuboid.h"

void NRRP3D(double *surfaces,int P,zone3D **res) ; //given the volumes (unsorted and that will not be modified during the execution) and their number P, returns the repartition using zone3D data structure (res).

#endif
