#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cuboid.h"
#include "subroutine.h"

double max3D(double x,double y){ 
  if (x >= y){
    return x ;
  }
  else{
    return y ;
  }
}

double min3D(double x,double y){
  if (x >= y){
    return y ;
  }
  else{
    return x ;
  }
}

double max3(double x, double y, double z){
  if(x >= y && x >= z){
    return x ;
      }
  else{
    if(y>=z){
      return y ;
    }
    else{
      return z ;
    }
  }
}

double min3(double x, double y, double z){
  if(x <= y && x <= z){
    return x ;
      }
  else{
    if(y>=z){
      return z ;
    }
    else{
      return y ;
    }
  }
}

double med(double x, double y, double z){
  if(x >= y && x >= z){
    return max3D(y,z) ;
      }
  else{
    if(y>=z){
      return max3D(x,z) ;
    }
    else{
      return max3D(x,y) ;
    }
  }
}

void RecPartNRRP3D(cuboid C,double *volumes,int debut, int fin,zone3D **res){
  if(debut == fin){
    zone3D *z ;
    z = malloc(sizeof(zone3D)) ;
    z -> head = C ;
    z -> queue = NULL ;
    res[debut] = z ;
  }
  else {
    double h,w,l,v ;
    double x1,x2,y1,y2,z1,z2 ;
    x1 = C.x1 ;
    x2 = C.x2 ;
    y1 = C.y1 ;
    y2 = C.y2 ;
    z1 = C.z1 ;
    z2 = C.z2 ;
    h = x2-x1 ;
    w = y2-y1 ;
    l = z2-z1 ;
    v= h*w*l ;
    double rho1 = max3(h,w,l)/min3(h,w,l) ;
    double rho2 = max3(h,w,l)/med(h,w,l) ;
    int k = 0;
    double v1 = 0.;
    while(v1 < v/(3*rho2)){
      v1 = v1 + volumes[debut+k] ; 
      k++ ;
    } ;
    if(k+debut <= fin){
      cuboid cubs[2] ;
      GuilloRec3D(C,v1/v,cubs) ;
      RecPartNRRP3D(cubs[0],volumes,debut,debut+k-1,res) ;
      RecPartNRRP3D(cubs[1],volumes,debut+k,fin,res) ; 
    }
    else{
      v1 = v-volumes[fin] ;
      if(rho1/rho2 > sqrt(3)){
	flatCube(C,v1/v,res,fin-1) ;
      }
      else{
	Cube(C,v1/v,res,fin-1) ;
      }
      RecPartNRRP3D(res[fin-1]->head,volumes,debut,fin-1,res) ;
    }
  }
}
      

void NRRP3D(double *volumes,int n,zone3D **res){
  cuboid C ;
  C.x1 = 0. ;
  C.x2 = 1. ;
  C.y1 = 0. ;
  C.y2 = 1. ;
  C.z1 = 0. ;
  C.z2 = 1. ;
  int cmp_func(const void *a,const void *b){
    double x, y ;
    x = volumes[*(int *) a];
    y = volumes[*(int *) b];
    if(fabs(x-y) < 1e-10) return 0; 
    if (x < y) return -1; 
    return 1; 
  } ;
  int *ind ;
  double *new_volumes ;
  ind = malloc(n*sizeof(int)) ;
  int i ;
  for(i=0;i<n;i++){
    ind[i] = i ;
  }
  zone3D **res_bis ;
  res_bis=malloc(n*sizeof(zone3D*)) ;
  new_volumes = malloc(n*sizeof(double)) ;
  qsort(ind,n,sizeof(int),cmp_func) ;
  for(i=0;i<n;i++){
    new_volumes[i] = volumes[ind[i]] ;
  }
  RecPartNRRP3D(C,new_volumes,0,n-1,res_bis) ;
  for(i=0;i<n;i++){
    res[ind[i]] = res_bis[i]  ;
  }
  free(ind) ;
  free(res_bis) ;
  free(new_volumes) ;
}


