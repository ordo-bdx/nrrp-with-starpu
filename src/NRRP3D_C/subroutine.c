#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cuboid.h"

void GuilloRec3D(cuboid cub,double alpha, cuboid res[2]){
  double h,w,l ;
  double x1,x2,y1,y2,z1,z2 ;
  x1 = cub.x1 ;
  x2 = cub.x2 ;
  y1 = cub.y1 ;
  y2 = cub.y2 ;
  z1 = cub.z1 ;
  z2 = cub.z2 ;
  h = x2-x1 ;
  w = y2-y1 ;
  l = z2-z1 ;
  cuboid C1,C2 ;
  if(h >= w && h>= l){   
    C1.x1 = x1 ;
    C1.x2 = x1+alpha*h ; 
    C1.y1 = y1 ;
    C1.y2 = y2 ;
    C1.z1 = z1 ;
    C1.z2 = z2 ;
    C2.x1 = x1+alpha*h ;
    C2.x2 = x2;
    C2.y1 = y1 ;
    C2.y2 = y2 ;
    C2.z1 = z1 ;
    C2.z2 = z2 ;
      }
  else{    
    if(w >= l){
      C1.x1 = x1 ;
      C1.x2 = x2 ;
      C1.y1 = y1 ;
      C1.y2 = y1 + alpha*w ;
      C1.z1 = z1 ;
      C1.z2 = z2 ;
      C2.x1 = x1 ;
      C2.x2 = x2 ;
      C2.y1 = y1+alpha*w ;
      C2.y2 = y2 ;
      C2.z1 = z1 ;
      C2.z2 = z2 ;
    }
    else{
      C1.x1 = x1 ;
      C1.x2 = x2 ;
      C1.y1 = y1 ;
      C1.y2 = y2 ;
      C1.z1 = z1 ;
      C1.z2 = z1 + alpha*l ;
      C2.x1 = x1 ;
      C2.x2 = x2 ;
      C2.y1 = y1 ;
      C2.y2 = y2 ;
      C2.z1 = z1+alpha*l ;
      C2.z2 = z2 ;
    }
  }
  res[0] = C1 ;
  res[1] = C2 ;
}

void Guillo3D(cuboid cub,double alpha, zone3D ** res, int debut){
  cuboid resbis[2] ;
  GuilloRec3D(cub,alpha,resbis) ;
  zone3D *z1,*z2 ;
  z1 = malloc(sizeof(zone3D)) ;
  z2 = malloc(sizeof(zone3D)) ;
  z1 -> head = resbis[0] ;
  z1 -> queue = NULL ;
  z2 -> head = resbis[1] ;
  z2 -> queue = NULL ;
  res[debut] = z1 ;
  res[debut+1] = z2 ;
}  

void Cube(cuboid cub, double alpha, zone3D **res,int debut){
  double h,w,l,v ;
  double x1,x2,y1,y2,z1,z2 ;
  x1 = cub.x1 ;
  x2 = cub.x2 ;
  y1 = cub.y1 ;
  y2 = cub.y2 ;
  z1 = cub.z1 ;
  z2 = cub.z2 ;
  h = x2-x1 ;
  w = y2-y1 ;
  l = z2 -z1 ;
  v = h*w*l ;
  zone3D * zo1,* zo2,* zo3, *zo4;
  zo1 = malloc(sizeof(zone3D)) ;
  zo2 = malloc(sizeof(zone3D)) ;
  zo3 = malloc(sizeof(zone3D)) ;
  zo4 = malloc(sizeof(zone3D)) ;
  cuboid C1,C2,C3,C4 ;
  C1.x1 = x1 ;
  C1.x2 = x1 + cbrt(alpha*v) ;
  C1.y1 = y1 ;
  C1.y2 = y1 + cbrt(alpha*v) ;
  C1.z1 = z1 ;
  C1.z2 = z1 + cbrt(alpha*v) ;
  
  C2.x1 = C1.x2 ;
  C2.x2 = x2 ;
  C2.y1 = y1 ;
  C2.y2 = C1.y2 ;
  C2.z1 = z1 ;
  C2.z2 = C1.z2 ;
  
  C3.x1 = x1 ;
  C3.x2 = x2 ;
  C3.y1 = y1 ;
  C3.y2 = C1.y2 ;
  C3.z1 = C1.z2 ;
  C3.z2 = z2 ;
  
  C4.x1 = x1 ;
  C4.x2 = x2 ;
  C4.y1 = C1.y2 ;
  C4.y2 = y2 ;
  C4.z1 = z1 ;
  C4.z2 = z2 ;

  zo1->head = C1 ;
  zo1->queue = NULL ;
  zo4->head = C4 ;
  zo4->queue = NULL ;
  zo3->head = C3 ;
  zo3->queue = zo4 ;
  zo2->head = C2 ;
  zo2->queue = zo3 ;
  res[debut] = zo1 ;
  res[debut+1] = zo2 ;
}

void flatCube(cuboid cub, double alpha, zone3D **res,int debut){
  double h,w,l;
  double x1,x2,y1,y2,z1,z2 ;
  x1 = cub.x1 ;
  x2 = cub.x2 ;
  y1 = cub.y1 ;
  y2 = cub.y2 ;
  z1 = cub.z1 ;
  z2 = cub.z2 ;
  h = x2-x1 ;
  w = y2-y1 ;
  l = z2 -z1 ;
  zone3D * zo1,* zo2,* zo3 ;
  zo1 = malloc(sizeof(zone3D)) ;
  zo2 = malloc(sizeof(zone3D)) ;
  zo3 = malloc(sizeof(zone3D)) ;
  cuboid C1,C2,C3 ;
  
  if(h<= l && h<=w){
    C1.x1 = x1 ;
    C1.x2 = x2 ;
    C1.y1 = y1 ;
    C1.y2 = y1+sqrt(alpha*w*l) ;
    C1.z1 = z1 ;
    C1.z2 = z1 +sqrt(alpha*w*l) ;

    C2.x1 = x1 ;
    C2.x2 = x2 ;
    C2.y1 = y1 ;
    C2.y2 = C1.y2 ;
    C2.z1 = C1.z2 ;
    C2.z2 = z2 ;

    C3.x1 = x1 ;
    C3.x2 = x2 ;
    C3.y1 = C1.y2 ;
    C3.y2 = y2 ;
    C3.z1 = z1 ;
    C3.z2 = z2 ; 
  }
  else{
  if(w<= l && w<=h){
    C1.x1 = x1 ;
    C1.x2 = x1+sqrt(alpha*h*l) ;
    C1.y1 = y1 ;
    C1.y2 = y2 ;
    C1.z1 = z1 ;
    C1.z2 = z1 +sqrt(alpha*h*l) ;

    C2.x1 = x1 ;
    C2.x2 = C1.x2 ;
    C2.y1 = y1 ;
    C2.y2 = y2 ;
    C2.z1 = C1.z2 ;
    C2.z2 = z2 ;

    C3.x1 = C1.x2 ;
    C3.x2 = x2 ;
    C3.y1 = y1 ;
    C3.y2 = y2 ;
    C3.z1 = z1 ;
    C3.z2 = z2 ; 
  }
  else{
    C1.x1 = x1 ;
    C1.x2 = x1+sqrt(alpha*h*w) ;
    C1.y1 = y1 ;
    C1.y2 = y1+sqrt(alpha*h*w) ;
    C1.z1 = z1 ;
    C1.z2 = z2 ;

    C2.x1 = x1 ;
    C2.x2 = C1.x2 ;
    C2.y1 = C1.y2 ;
    C2.y2 = y2 ;
    C2.z1 = z1 ;
    C2.z2 = z2 ;

    C3.x1 = C1.x2 ;
    C3.x2 = x2 ;
    C3.y1 = y1 ;
    C3.y2 = y2 ;
    C3.z1 = z1 ;
    C3.z2 = z2 ; 
  }
  }

  zo1->head = C1 ;
  zo1->queue = NULL ;
  zo3->head = C3 ;
  zo3->queue = NULL ;
  zo2->head = C2 ;
  zo2->queue = zo3 ;
  res[debut] = zo1 ;
  res[debut+1] = zo2 ;
}

