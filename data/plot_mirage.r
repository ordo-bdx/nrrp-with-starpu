library(ggplot2)
library(reshape2)
library(plyr)
# library(Hmisc)

t <- read.table("results.mirage01", col.names=c("N", "l", "strat", "param", "alg", "round", "redux", "id", "alloc", "time", "comm"), stringsAsFactors=FALSE)
t$N <- factor(t$N,levels=c(7680,15360,23040,30720),labels=c("N = 7680","N = 15360","N = 23040","N = 30720"))
t$round <- factor(t$round,levels=c("Coarse","Precise"),labels=c("Rounded","Precise"))
t[is.na(t$alg), "redux"] <- "NoRedux"
t[t$alg %in% c("NRRP"), "redux"] <- "NoRedux"
t[t$alg %in% c("NRRP3D"), "alg"] <- "3D-NRRP"
t[t$strat %in% c("SChoice"), "param"] <- -1
t[t$param != -1,"strat"] <- paste(t[t$param != -1,"strat"], t[t$param != -1, "param"], sep="-")
t[!is.na(t$round), "alg"] <- paste(t[!is.na(t$round), "alg"], t[!is.na(t$round), "round"], sep='-')
t[t$redux == "Redux", "alg"] <- paste(t[t$redux == "Redux", "alg"], t[t$redux == "Redux", "redux"], sep='-')
t[is.na(t$alg),"alg"] <- "Dynamic" 
t$alg <- factor(t$alg,levels=c("Dynamic","NRRP-Rounded","NRRP-Precise","3D-NRRP-Rounded","3D-NRRP-Precise","3D-NRRP-Rounded-Redux","3D-NRRP-Precise-Redux"), labels=c("Dynamic","NRRP-Rounded","NRRP-Precise","3D-Rounded","3D-Precise","3D-Rounded-Redux","3D-Precise-Redux"))
t$strat <- factor(t$strat,levels=c("Dyn","DChoice-10","DChoice-50","DCheaper","DMDA","Stat","SRand","SChoice","SCheaper"),labels=c("FirstDyn","ChoiceDyn-10","ChoiceDyn-50","EffectiveDyn","DMDA","Static","RandSteal","ChoiceSteal","EffectiveSteal"))

colors = c("blue", "green", "red", "orange", "magenta", "brown", "orchid4", "gold", "gray")

ratio <- 0.6

p <- ggplot(t[t$strat != "FirstDyn" &  !(t$alg %in% c("3D-Rounded","3D-Precise")),], aes(x=strat, y = time, color = strat))  + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Makespan (s)") + xlab("Strategies")  + guides(colour="none") + scale_color_manual(values = c("blue", "green", "red", "orange", "magenta", "brown", "orchid4", "gold")) 
ggsave("time_mirage01_onlyRedux.pdf", p, width = ratio * 10.3, height = ratio * 7.38)


p <- ggplot(t[t$strat != "FirstDyn" &  !(t$alg %in% c("3D-Rounded","3D-Precise")),], aes(x=strat, y = comm, color = strat))  + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Communication (GB)") + xlab("Strategies") + guides(colour="none") + scale_color_manual(values = c("blue", "green", "red", "orange", "magenta", "brown", "orchid4", "gold"))
ggsave("comm_mirage01_onlyRedux.pdf", p, width = ratio * 10.3, height = ratio * 7.38)

p <- ggplot(t[t$alg %in% c("3D-Precise","3D-Precise-Redux","NRRP-Precise"),], aes(x=strat, y = time, color = strat))  + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Makespan (s)") + xlab("Strategies") + guides(colour="none") + scale_color_manual(values = c("magenta", "brown", "orchid4", "gold"))
ggsave("time_mirage01_ReduxVsNo.pdf", p, width = ratio * 7.04, height = ratio * 7)

p <- ggplot(t[t$alg %in% c("3D-Precise","3D-Precise-Redux","NRRP-Precise"),], aes(x=strat, y = comm, color = strat))  + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Communication (GB)") + xlab("Strategies") + guides(colour="none") + scale_color_manual(values = c("magenta", "brown", "orchid4", "gold"))
ggsave("comm_mirage01_ReduxVsNo.pdf", p, width = ratio * 7.04, height = ratio * 7)

p <- ggplot(t[t$alg == "Dynamic",], aes(x=strat, y = time, color = strat)) + stat_summary(geom="point", fun.y=mean) + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Makespan (s)") + xlab("Strategies") + guides(colour="none") + scale_color_manual(values = c( "black", "blue", "green", "red", "orange"))
ggsave("time_mirage01_dyn.pdf", p, width = ratio * 4, height = ratio * 7)

p <- ggplot(t[t$alg == "Dynamic",,], aes(x=strat, y = comm, color = strat)) + stat_summary(geom="point", fun.y=mean) + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Communication (GB)") + xlab("Strategies") + guides(colour="none") + scale_color_manual(values = c( "black", "blue", "green", "red", "orange"))
ggsave("comm_mirage01_dyn.pdf", p, width = ratio * 4, height = ratio * 7)

p <- ggplot(t[t$strat != "FirstDyn" &  !(t$alg %in% c("3D-Rounded","3D-Precise","3D-Rounded-Redux","3D-Precise-Redux")),], aes(x=strat, y = time, color = strat))  + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Makespan (s)") + xlab("Strategies")  + guides(colour="none") + scale_color_manual(values = c("blue", "green", "red", "orange", "magenta", "brown", "orchid4", "gold")) 
ggsave("time_mirage01_only2D.pdf", p, width = ratio * 10.1, height = ratio * 6.26)


p <- ggplot(t[t$strat != "FirstDyn" &  !(t$alg %in% c("3D-Rounded","3D-Precise","3D-Rounded-Redux","3D-Precise-Redux")),], aes(x=strat, y = comm, color = strat))  + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Communication (GB)") + xlab("Strategies") + guides(colour="none") + scale_color_manual(values = c("blue", "green", "red", "orange", "magenta", "brown", "orchid4", "gold"))
ggsave("comm_mirage01_only2D.pdf", p, width = ratio * 10.1, height = ratio * 6.26)


p <- ggplot(t[t$strat != "RandSteal" & t$strat != "ChoiceSteal" & t$strat != "FirstDyn" & !(t$alg %in% c("3D-Rounded","3D-Rounded-Redux","NRRP-Rounded","3D-Precise")),], aes(x=strat, y = time, color = strat))  + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Makespan (s)") + xlab("Strategies")  + guides(colour="none") + scale_color_manual(values = c("blue", "green", "red", "orange", "magenta", "gold"))
ggsave("time_mirage01_precise.pdf", p, width = ratio * 8.76, height = ratio * 6.83)

p <- ggplot(t[t$strat != "RandSteal" & t$strat != "ChoiceSteal" & t$strat != "FirstDyn" & !(t$alg %in% c("3D-Rounded","3D-Precise-Redux","NRRP-Precise","3D-Precise")),], aes(x=strat, y = time, color = strat))  + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Makespan (s)") + xlab("Strategies")  + guides(colour="none") + scale_color_manual(values = c("blue", "green", "red", "orange", "magenta", "gold"))
ggsave("time_mirage01_rounded.pdf", p, width = ratio * 8.76, height = ratio * 6.83)

p <- ggplot(t[t$strat != "RandSteal" & t$strat != "ChoiceSteal" & t$strat != "FirstDyn" & !(t$alg %in% c("3D-Rounded","3D-Rounded-Redux","NRRP-Rounded","3D-Precise")),], aes(x=strat, y = comm, color = strat))  + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Communication (GB)") + xlab("Strategies")  + guides(colour="none") + scale_color_manual(values = c("blue", "green", "red", "orange", "magenta", "gold"))
ggsave("comm_mirage01_precise.pdf", p, width = ratio * 8.76, height = ratio * 6.83)

p <- ggplot(t[t$strat != "RandSteal" & t$strat != "ChoiceSteal" & t$strat != "FirstDyn" & !(t$alg %in% c("3D-Rounded","3D-Precise-Redux","NRRP-Precise","3D-Precise")),], aes(x=strat, y = comm, color = strat))  + geom_boxplot() + facet_grid(N~alg, scales="free") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Communication (GB)") + xlab("Strategies")  + guides(colour="none") + scale_color_manual(values = c("blue", "green", "red", "orange", "magenta", "gold"))
ggsave("comm_mirage01_rounded.pdf", p, width = ratio * 8.76, height = ratio * 6.83)

