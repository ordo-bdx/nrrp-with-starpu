library(ggplot2)
library(reshape2)
library(plyr)
library(Hmisc)

t <- read.table("blocks.sirocco04", col.names=c("N", "l", "strat", "param", "alg", "round", "redux", "id", "alloc", "time", "comm"), stringsAsFactors=FALSE)
t$N <- factor(t$N,levels=c(7680,15360,23040,30720),labels=c("N = 7680","N = 15360","N = 23040","N = 30720"))
t$l <- factor(t$l,levels=c(480,960,1920),labels=c("l = 480","l = 960","l = 1920"))
#t$round <- factor(t$round,levels=c("Coarse","Precise"),labels=c("","Precise"))
t[is.na(t$alg), "redux"] <- "NoRedux"
t[t$alg %in% c("NRRP"), "redux"] <- "NoRedux"
#t[t$alg %in% c("NRRP3D"), "alg"] <- "3D-NRRP"
#t[t$strat %in% c("SChoice"), "param"] <- -1
t[t$param != -1,"strat"] <- paste(t[t$param != -1,"strat"], t[t$param != -1, "param"], sep="-")
t[!is.na(t$round), "alg"] <- paste(t[!is.na(t$round), "alg"], t[!is.na(t$round), "round"], sep='')
t[t$redux == "Redux", "alg"] <- paste(t[t$redux == "Redux", "alg"], t[t$redux == "Redux", "redux"], sep='-')
t[is.na(t$alg),"alg"] <- "Dynamic" 
t$alg <- factor(t$alg,levels=c("Dynamic","NRRP","NRRPPrecise","3D-NRRP","3D-NRRPPrecise","3D-NRRP-Redux","3D-NRRPPrecise-Redux"))
t$strat <- factor(t$strat,levels=c("Dyn","DChoice-10","DChoice-50","DCheaper","DMDA","Stat","SRand","SChoice","SCheaper"),labels=c("FirstDyn","ChoiceDyn-10","ChoiceDyn-50","EffectiveDyn","DMDA","Static","RandSteal","ChoiceSteal","EffectiveSteal"))

colors = c("blue", "green", "red", "orange", "magenta", "brown", "orchid4", "gold", "gray")

p <- ggplot(t, aes(x=strat, y = time, color = strat))  + geom_boxplot() + facet_grid(N~l, scales="free",space = "free_x") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Makespan (s)") + xlab("Strategies") + guides(colour="none") + scale_color_manual(values = c("red","magenta", "orchid4", "gold"))
ggsave("makespan_blocks.pdf",p,width=11.5,height=6.01)

p <- ggplot(t, aes(x=strat, y = comm, color = strat))  + geom_boxplot() + facet_grid(N~l, scales="free",space = "free_x") + theme(axis.text.x=element_text(angle=45,hjust=1)) + ylab("Communication (GB)") + xlab("Strategies") + guides(colour="none") + scale_color_manual(values = c("red","magenta", "orchid4", "gold"))
ggsave("comms_blocks.pdf",p,width=11.5,height=6.01)

