library(ggplot2)
library(reshape2)
library(plyr)
# library(Hmisc)

read_data <- function(name) {

t <- read.table(name, col.names=c("N", "l", "strat", "param", "alg", "round", "redux", "id", "alloc", "time", "comm"), stringsAsFactors=FALSE)
t$N <- factor(t$N,levels=c(7680,15360,23040,30720),labels=c("N = 7680","N = 15360","N = 23040","N = 30720"))
t$round <- factor(t$round,levels=c("Coarse","Precise"),labels=c("Rounded","Precise"))
t[is.na(t$alg), "redux"] <- "NoRedux"
t[t$alg %in% c("NRRP"), "redux"] <- "NoRedux"
t[t$alg %in% c("NRRP3D"), "alg"] <- "3D-NRRP"
t[t$strat %in% c("SChoice"), "param"] <- -1
t[t$param != -1,"strat"] <- paste(t[t$param != -1,"strat"], t[t$param != -1, "param"], sep="-")
t[!is.na(t$round), "alg"] <- paste(t[!is.na(t$round), "alg"], t[!is.na(t$round), "round"], sep='-')
t[t$redux == "Redux", "alg"] <- paste(t[t$redux == "Redux", "alg"], t[t$redux == "Redux", "redux"], sep='-')
t[is.na(t$alg),"alg"] <- "Dynamic"
t[is.na(t$strat), "strat"] <- "None"
t$alg <- factor(t$alg,levels=c("Dynamic","NRRP-Rounded","NRRP-Precise","3D-NRRP-Rounded","3D-NRRP-Precise","3D-NRRP-Rounded-Redux","3D-NRRP-Precise-Redux"), labels=c("Dynamic","NRRP-Rounded","NRRP-Precise","3D-Rounded","3D-Precise","3D-Rounded-Redux","3D-Precise-Redux"))
t$strat <- factor(t$strat,levels=c("Dyn","DChoice-10","DChoice-50","DCheaper","DMDA","Stat","SRand","SChoice","SCheaper", "chameleon"),labels=c("FirstDyn","ChoiceDyn-10","ChoiceDyn-50","EffectiveDyn","DMDA","Static","RandSteal","ChoiceSteal","EffectiveSteal", "Chameleon"))

return(t);
}

colors = c("FirstDyn"="gold",
       "ChoiceDyn-10" = "blue",
       "ChoiceDyn-50" = "green",
       "EffectiveDyn" = "red",
       "DMDA" = "black",
       "Static" = "magenta",
       	"RandSteal" = "brown",
	"ChoiceSteal" = "orchid4",
	"EffectiveSteal" = "orange",
	"Chameleon" = "gray",
	"NRRP" = "magenta",
	"NRRP+Vol" = "gold" )

t <- read_data("results.sirocco04.all")

tsumm <- ddply(t, ~N+strat+alg+l, summarize,
      	       timeM = mean(time), timemin = min(time), timemax=max(time),
      	       commM = mean(comm), commmin = min(comm), commmax=max(comm))
	       


plot_boxplot <- function(p,ylabel) {
# p <- p + stat_summary(geom="point", fun.y=mean, aes(x=strat, y = time, color = strat))
q <- p + geom_boxplot()
q <- q + facet_grid(N~alg, scales="free")
q <- q + theme(axis.text.x=element_text(angle=45,hjust=1))
q <- q + ylab(ylabel)
# q <- q + xlab("Strategies")
q <- q + xlab("")
q <- q + guides(colour="none")
q <- q + scale_color_manual(values = colors)
return(q)
}

plot_rect <- function(p, title) {
#p <- p + geom_errorbar() + geom_errorbarh()
q <- p + geom_rect(aes(xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat), fill=NA) + geom_point(aes(x=commM, y = timeM, color=strat))
q <- q + facet_wrap(~N, scales="free", nrow=2)
q <- q + ggtitle(title)
q <- q + ylab("Execution time (s)") + xlab("Communication (GB)")
q <- q + theme(legend.position="bottom")
q <- q + scale_color_manual(values = colors, name="")
return(q)
}

args <- commandArgs(trailingOnly = TRUE)

if("all" %in% args) {
args <- c(args, "dyn", "steal", "rounding", "3D", "overall", "mirage","cpu","sirocco05","miragebis","cpubis")
}

ratio <- 0.6
height <- 8.

ratiobeamer <- 0.7
heightbeamer <- 7.5*ratiobeamer
widthbeamer <- 13.75*ratiobeamer


## Dynamic strategies
if("dyn" %in% args) {

p <- ggplot(t[t$alg == "Dynamic" & ! t$strat %in% c("FirstDyn"),], aes(x=strat, y = time, color = strat))
p <- plot_boxplot(p, "Execution time (s)")
ggsave("time_sirocco04_dyn.pdf", p, width = ratio * 4, height = ratio * 7)

p <- ggplot(t[t$alg == "Dynamic"& ! t$strat %in% c("FirstDyn"),], aes(x=strat, y = comm, color = strat))
p <- plot_boxplot(p, "Communication (GB)")
ggsave("comm_sirocco04_dyn.pdf", p, width = ratio * 4, height = ratio * 7)

p <- ggplot(tsumm[tsumm$alg == "Dynamic" & ! tsumm$strat %in% c("FirstDyn"),])
p <- plot_rect(p, "")
### p <- p + geom_point(data = t[t$alg == "Dynamic" & ! t$strat %in% c("FirstDyn"),], aes(x=comm, y = time, color = strat), alpha = 0.25, size=0.2)
ggsave("rect_sirocco04_dyn.pdf", p, width = ratio * 7.1, height = ratio * 8.5)
}

### Stealing strategies

if("steal" %in% args) {

p <- ggplot(t[t$strat != "FirstDyn" &  !(t$alg %in% c("3D-Rounded","3D-Precise","3D-Rounded-Redux","3D-Precise-Redux", "Dynamic")),], aes(x=strat, y = time, color = strat))
p <- plot_boxplot(p, "Execution time (s)")
ggsave("time_sirocco04_steal.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(t[t$strat != "FirstDyn" &  !(t$alg %in% c("3D-Rounded","3D-Precise","3D-Rounded-Redux","3D-Precise-Redux", "Dynamic")),], aes(x=strat, y = comm, color = strat))
p <- plot_boxplot(p, "Communication (GB)")
ggsave("comm_sirocco04_steal.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(tsumm[tsumm$strat != "FirstDyn" &  tsumm$alg == "NRRP-Rounded",], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat))
p <- plot_rect(p, "NRRP-Rounded")
p <- p + guides(color = guide_legend(title="",nrow = 2))
ggsave("rect_sirocco04_Rounded.pdf", p, width = ratio * 7.1, height = ratio * 8.5)

p <- ggplot(tsumm[tsumm$strat != "FirstDyn" &  tsumm$alg == "NRRP-Precise",], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat))
p <- plot_rect(p, "NRRP-Precise")
p <- p + guides(color = guide_legend(title="",nrow = 2))
ggsave("rect_sirocco04_Precise.pdf", p, width = ratio * 7.1, height = ratio * 8.5)

}

### Rounded vs Precise

if("rounding" %in% args) {

p <- ggplot(t[(t$alg %in% c("NRRP-Rounded","NRRP-Precise") & t$strat %in% c("Static", "EffectiveSteal")),], aes(x=strat, y = time, color = strat))
p <- plot_boxplot(p, "Execution time (s)")
ggsave("time_sirocco04_rounding.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(t[(t$alg %in% c("NRRP-Rounded","NRRP-Precise") & t$strat %in% c("Static", "EffectiveSteal")),], aes(x=strat, y = comm, color = strat))
p <- plot_boxplot(p, "Communication (GB)")
ggsave("comm_sirocco04_rounding.pdf", p, width = ratio * 6.1, height = ratio * 6.26)


p <- ggplot(tsumm[tsumm$alg %in% c("NRRP-Rounded","NRRP-Precise") & tsumm$strat %in% c("Static", "EffectiveSteal"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=alg, shape=alg))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title=""), shape = guide_legend(title=""))
ggsave("rect_sirocco04_rounding.pdf", p, width = ratio * 7.1, height = ratio * 8.5)

}


### 3D versus 2D

if("3D" %in% args) {

p <- ggplot(t[(t$alg %in% c("NRRP-Rounded","3D-Rounded", "3D-Rounded-Redux") & t$strat %in% c("Static", "EffectiveSteal")),], aes(x=strat, y = time, color = strat))
p <- plot_boxplot(p, "Execution time (s)")
ggsave("time_sirocco04_3Dvs2D.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(t[(t$alg %in% c("NRRP-Rounded","3D-Rounded", "3D-Rounded-Redux") & t$strat %in% c("Static", "EffectiveSteal")),], aes(x=strat, y = comm, color = strat))
p <- plot_boxplot(p, "Communication (GB)")
ggsave("comm_sirocco04_3Dvs2D.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(tsumm[tsumm$alg %in% c("NRRP-Rounded","3D-Rounded", "3D-Rounded-Redux") & tsumm$strat %in% c("Static", "EffectiveSteal"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=alg, shape=alg))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title=""), shape = guide_legend(title=""))
ggsave("rect_sirocco04_3Dvs2D.pdf", p, width = ratio * 7.1, height = ratio * 8.5)


}

### Overall comparison

if("overall" %in% args) {

tcham <- read_data("results.chameleon.indiv")
## print(head(tcham))
t <- rbind(t, tcham)
tsumm <- ddply(t, ~N+strat+alg+l, summarize,
      	       timeM = mean(time), timemin = min(time), timemax=max(time),
      	       commM = mean(comm), commmin = min(comm), commmax=max(comm))


p <- ggplot(t[(t$alg %in% c("NRRP-Rounded", "Dynamic") & t$strat %in% c("Static", "EffectiveSteal", "DMDA", "ChoiceDyn-50", "Chameleon")),], aes(x=strat, y = time, color = strat))
p <- plot_boxplot(p, "Execution time (s)")
ggsave("time_sirocco04_overall.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(t[(t$alg %in% c("NRRP-Rounded", "Dynamic") & t$strat %in% c("Static", "EffectiveSteal", "DMDA", "ChoiceDyn-50", "Chameleon")),], aes(x=strat, y = comm, color = strat))
p <- plot_boxplot(p, "Communication (GB)")
ggsave("comm_sirocco04_overall.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(tsumm[tsumm$alg %in% c("NRRP-Rounded","Dynamic") & tsumm$strat %in% c("Static", "EffectiveSteal", "DMDA", "ChoiceDyn-50", "Chameleon"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=alg, shape=alg))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title=""), shape = guide_legend(title=""), color = guide_legend(title="",nrow = 2))
ggsave("rect_sirocco04_overall.pdf", p, width = ratio * 7.1, height = ratio * 8.5)

}

if("sirocco05" %in% args) {

t <- read_data("results.sirocco05.all")

tsumm <- ddply(t, ~N+strat+alg+l, summarize,
      	       timeM = mean(time), timemin = min(time), timemax=max(time),
      	       commM = mean(comm), commmin = min(comm), commmax=max(comm))

p <- ggplot(tsumm[tsumm$alg == "Dynamic" & ! tsumm$strat %in% c("FirstDyn"),])
p <- plot_rect(p, "")
p <- p + guides(color = guide_legend(title="",nrow = 2))
### p <- p + geom_point(data = t[t$alg == "Dynamic" & ! t$strat %in% c("FirstDyn"),], aes(x=comm, y = time, color = strat), alpha = 0.25, size=0.2)
ggsave("rect_sirocco05_dyn.pdf", p, width = ratio * 7.1, height = ratio * height)

p <- ggplot(tsumm[tsumm$strat != "FirstDyn" &  tsumm$alg == "NRRP-Rounded",], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat))
p <- plot_rect(p, "NRRP-Rounded")
p <- p + guides(linetype = guide_legend(title="",order=1), shape = guide_legend(title="",order=1), color = guide_legend(title="",nrow = 2,order=2))
ggsave("rect_sirocco05_Rounded.pdf", p, width = ratio * 7.1, height = ratio * height)

p <- ggplot(tsumm[tsumm$strat != "FirstDyn" &  tsumm$alg == "NRRP-Rounded" & tsumm$strat != "ChoiceSteal",], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat))
p <- plot_rect(p, "NRRP-Rounded")
p <- p + guides(color = guide_legend(title="",nrow = 1))
ggsave("rect_sirocco05_Rounded_beamer.pdf", p, width = widthbeamer, height = heightbeamer)

p <- ggplot(tsumm[tsumm$strat != "FirstDyn" &  tsumm$alg == "NRRP-Precise",], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat))
p <- plot_rect(p, "NRRP-Precise")
p <- p + guides(linetype = guide_legend(title="",order=1), shape = guide_legend(title="",order=1), color = guide_legend(title="",nrow = 2,order=2))
ggsave("rect_sirocco05_Precise.pdf", p, width = ratio * 7.1, height = ratio * height)

p <- ggplot(tsumm[tsumm$alg %in% c("NRRP-Rounded","NRRP-Precise") & tsumm$strat %in% c("Static", "EffectiveSteal"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=alg, shape=alg))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title="",order=1), shape = guide_legend(title="",order=1), color = guide_legend(title="",order=2))
ggsave("rect_sirocco05_rounding.pdf", p, width = ratio * 7.1, height = ratio * height)

p <- ggplot(tsumm[tsumm$alg %in% c("NRRP-Rounded","3D-Rounded", "3D-Rounded-Redux") & tsumm$strat %in% c("Static", "EffectiveSteal") & !(tsumm$alg == "3D-Rounded" & tsumm$strat == "Static"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=alg, shape=alg))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title="",nrow=2,order=1), shape = guide_legend(title="",nrow=2,order=1),color = guide_legend(title="",order=2))
ggsave("rect_sirocco05_3Dvs2D.pdf", p, width = ratio * 7.1, height = ratio * height)

p <- ggplot(tsumm[tsumm$alg %in% c("NRRP-Rounded","3D-Rounded", "3D-Rounded-Redux") & tsumm$strat %in% c("Static", "EffectiveSteal") & !(tsumm$alg == "3D-Rounded" & tsumm$strat == "Static"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=alg, shape=alg))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title="",nrow=1,order=1), shape = guide_legend(title="",nrow=2,order=1),color = guide_legend(title="",order=2))
ggsave("rect_sirocco05_3Dvs2D_beamer.pdf", p, width = widthbeamer, height = heightbeamer)

tcham <- read_data("results.chameleon.indiv")
## print(head(tcham))
t <- rbind(t, tcham)
tsumm <- ddply(t, ~N+strat+alg+l, summarize,
      	       timeM = mean(time), timemin = min(time), timemax=max(time),
      	       commM = mean(comm), commmin = min(comm), commmax=max(comm))

p <- ggplot(tsumm[tsumm$alg %in% c("NRRP-Rounded","Dynamic") & tsumm$strat %in% c("Static", "EffectiveSteal", "DMDA", "ChoiceDyn-50", "Chameleon"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=alg, shape=alg))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title="",order=1), shape = guide_legend(title="",order=1), color = guide_legend(title="",nrow = 2,order=2))
ggsave("rect_sirocco05_overall.pdf", p, width = ratio * 7.1, height = ratio * height)

colors = c("FirstDyn"="gold",
       "ChoiceDyn-10" = "blue",
       "ChoiceDyn-50" = "green",
       "EffectiveDyn" = "red",
       "DMDA" = "black",
       "Static" = "magenta",
       	"RandSteal" = "brown",
	"ChoiceSteal" = "orchid4",
	"EffectiveSteal" = "orange",
	"Chameleon" = "red",
	"NRRP" = "magenta",
	"NRRP+Vol" = "gold" )

p <- ggplot(tsumm[tsumm$alg %in% c("NRRP-Rounded","Dynamic") & tsumm$strat %in% c("Static", "EffectiveSteal", "DMDA", "Chameleon"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=alg, shape=alg))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title="",order=1), shape = guide_legend(title="",order=1), color = guide_legend(title="",nrow = 1,order=2))
ggsave("rect_sirocco05_overall_beamer.pdf", p, width = widthbeamer, height = heightbeamer)

colors = c("FirstDyn"="gold",
       "ChoiceDyn-10" = "blue",
       "ChoiceDyn-50" = "green",
       "EffectiveDyn" = "red",
       "DMDA" = "black",
       "Static" = "magenta",
       	"RandSteal" = "brown",
	"ChoiceSteal" = "orchid4",
	"EffectiveSteal" = "orange",
	"Chameleon" = "gray",
	"NRRP" = "magenta",
	"NRRP+Vol" = "gold" )


tsumm <- ddply(tsumm, ~N+l, transform,
      timeRatio = timeM/timeM[strat=="Chameleon"],
      commRatio = commM/commM[strat=="Chameleon"])

options(width=10000)
print(dcast(melt(tsumm[tsumm$alg %in% c("Dynamic", "NRRP-Rounded") & tsumm$strat %in% c("EffectiveSteal", "Chameleon"), c("N", "alg", "strat", "timeM", "timeRatio", "commM", "commRatio")], id.vars=c("N", "alg", "strat")), N~alg+strat+variable))

tsumm <- ddply(t, ~N+strat+alg+l, summarize,
      	       timeM = mean(time), timemin = min(time), timemax=max(time),
      	       commM = mean(comm), commmin = min(comm), commmax=max(comm))

tsumm$strat <- factor(tsumm$strat,levels=c("FirstDyn","ChoiceDyn-10","ChoiceDyn-50","EffectiveDyn","Static","RandSteal","ChoiceSteal","EffectiveSteal","DMDA", "Chameleon"),labels=c("FirstDyn","ChoiceDyn-10","ChoiceDyn-50","EffectiveDyn","NRRP","RandSteal","ChoiceSteal","NRRP+Vol","DMDA", "Chameleon"))

p <- ggplot(tsumm[tsumm$alg %in% c("NRRP-Rounded","Dynamic") & tsumm$strat %in% c("NRRP", "NRRP+Vol", "DMDA", "Chameleon"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat))
p <- plot_rect(p, "")
p <- p + theme(legend.position = "right", legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(color = guide_legend(title=""))
ggsave("rect_sirocco05_overall_aud.pdf", p, width = ratio * 11., height = ratio * 7.1)

}

### Mirage plots

if("mirage" %in% args) {
  tmirage <- read_data("results.mirage01")

  tmiragesumm <- ddply(tmirage, ~N+strat+alg+l, summarize,
      	       timeM = mean(time), timemin = min(time), timemax=max(time),
      	       commM = mean(comm), commmin = min(comm), commmax=max(comm))

p <- ggplot(tmirage[(tmirage$alg %in% c("NRRP-Rounded", "Dynamic") & tmirage$strat %in% c("Static", "EffectiveSteal", "DMDA", "ChoiceDyn-50")),], aes(x=strat, y = time, color = strat))
p <- plot_boxplot(p, "Execution time (s)")
ggsave("time_mirage_overall.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(tmirage[(tmirage$alg %in% c("NRRP-Rounded", "Dynamic") & tmirage$strat %in% c("Static", "EffectiveSteal", "DMDA", "ChoiceDyn-50")),], aes(x=strat, y = comm, color = strat))
p <- plot_boxplot(p, "Communication (GB)")
ggsave("comm_mirage_overall.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(tmiragesumm[tmiragesumm$alg %in% c("NRRP-Rounded","NRRP-Precise", "Dynamic") & tmiragesumm$strat %in% c("Static", "EffectiveSteal", "DMDA", "ChoiceDyn-50"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=alg, shape=alg))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title=""), shape = guide_legend(title=""))
ggsave("rect_mirage_overall.pdf", p, width = ratio * 7.1, height = ratio * 8.5)
 
}

if("miragebis" %in% args) {
  tmirage <- read_data("results.mirage01_bis")

  tmiragesumm <- ddply(tmirage, ~N+strat+alg+l, summarize,
      	       timeM = mean(time), timemin = min(time), timemax=max(time),
      	       commM = mean(comm), commmin = min(comm), commmax=max(comm))

p <- ggplot(tmirage[(tmirage$alg %in% c("NRRP-Rounded", "Dynamic") & tmirage$strat %in% c("Static", "EffectiveSteal", "DMDA", "ChoiceDyn-50")),], aes(x=strat, y = time, color = strat))
p <- plot_boxplot(p, "Execution time (s)")
ggsave("time_miragebis_overall.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(tmirage[(tmirage$alg %in% c("NRRP-Rounded", "Dynamic") & tmirage$strat %in% c("Static", "EffectiveSteal", "DMDA", "ChoiceDyn-50")),], aes(x=strat, y = comm, color = strat))
p <- plot_boxplot(p, "Communication (GB)")
ggsave("comm_miragebis_overall.pdf", p, width = ratio * 6.1, height = ratio * 6.26)

p <- ggplot(tmiragesumm[tmiragesumm$alg %in% c("NRRP-Rounded","NRRP-Precise", "Dynamic") & tmiragesumm$strat %in% c("Static", "EffectiveSteal", "DMDA", "EffectiveDyn", "ChoiceDyn-50") & !(tmiragesumm$alg == "NRRP-Rounded" & tmiragesumm$strat == "Static"),], aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=alg, shape=alg))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title="",order=1), shape = guide_legend(title="",order=1), color = guide_legend(title="",nrow = 2,order=2))
ggsave("rect_miragebis_overall.pdf", p, width = ratio * 7.1, height = ratio * height)
 
}

if ("cpu" %in% args) {
   t <- read.table("sirocco04_use_CPU", col.names=c("N", "l", "strat", "param", "alg", "round", "redux", "id", "alloc", "time", "comm"), stringsAsFactors=FALSE)
t$N <- factor(t$N,levels=c(7680,15360,23040,30720),labels=c("N = 7680","N = 15360","N = 23040","N = 30720"))
t[is.na(t$alg), "redux"] <- "NoRedux"
t[t$alg %in% c("NRRP"), "redux"] <- "NoRedux"
t[t$param != -1,"strat"] <- paste(t[t$param != -1,"strat"], t[t$param != -1, "param"], sep="-")
t[!is.na(t$round), "alg"] <- paste(t[!is.na(t$round), "alg"], t[!is.na(t$round), "round"], sep='-')
t[t$redux == "Redux", "alg"] <- paste(t[t$redux == "Redux", "alg"], t[t$redux == "Redux", "redux"], sep='-')
t[is.na(t$alg),"alg"] <- "Dynamic" 
t$alg <- factor(t$alg,levels=c("Dynamic","NRRP","NRRP-Precise","3D-NRRP","3D-NRRP-Precise","3D-NRRP-Redux","3D-NRRP-Precise-Redux"))
t$withcpu <- grepl("With_CPU", t$strat)
t$withcpu <- factor(t$withcpu, levels=c(TRUE, FALSE), labels=c("With CPUs", "Without CPUs"))
t$strat <- lapply(strsplit(t$strat, "_"), `[[`, 1)
t$strat <- factor(t$strat,levels=c("Dyn","DChoice-10","DChoice-50","DCheaper","DMDA","Stat","SRand","SChoice","SCheaper", "chameleon"),labels=c("FirstDyn","ChoiceDyn-10","ChoiceDyn-50","EffectiveDyn","DMDA","Static","RandSteal","ChoiceSteal","EffectiveSteal", "Chameleon"))

  tsumm <- ddply(t, ~N+strat+alg+l+withcpu, summarize,
      	       timeM = mean(time), timemin = min(time), timemax=max(time),
      	       commM = mean(comm), commmin = min(comm), commmax=max(comm))

p <- ggplot(tsumm, aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=withcpu, shape=withcpu))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title=""), shape = guide_legend(title=""))
ggsave("rect_cpu.pdf", p, width = ratio * 7.1, height = ratio * 8.5)
}

if ("cpubis" %in% args) {
   t <- read.table("sirocco05_use_CPU", col.names=c("N", "l", "strat", "param", "alg", "round", "redux", "id", "alloc", "time", "comm"), stringsAsFactors=FALSE)
t$N <- factor(t$N,levels=c(7680,15360,23040,30720),labels=c("N = 7680","N = 15360","N = 23040","N = 30720"))
t[is.na(t$alg), "redux"] <- "NoRedux"
t <- t[is.na(t$round) | t$round == "Coarse" ,]
t[t$alg %in% c("NRRP"), "redux"] <- "NoRedux"
t[t$param != -1,"strat"] <- paste(t[t$param != -1,"strat"], t[t$param != -1, "param"], sep="-")
t[!is.na(t$round), "alg"] <- paste(t[!is.na(t$round), "alg"], t[!is.na(t$round), "round"], sep='-')
t[t$redux == "Redux", "alg"] <- paste(t[t$redux == "Redux", "alg"], t[t$redux == "Redux", "redux"], sep='-')
t[is.na(t$alg),"alg"] <- "Dynamic" 
t$alg <- factor(t$alg,levels=c("Dynamic","NRRP","NRRP-Precise","3D-NRRP","3D-NRRP-Precise","3D-NRRP-Redux","3D-NRRP-Precise-Redux"))
t$withcpu <- grepl("With_CPU", t$strat)
t$withcpu <- factor(t$withcpu, levels=c(TRUE, FALSE), labels=c("With CPUs", "Without CPUs"))
t$strat <- lapply(strsplit(t$strat, "_"), `[[`, 1)
t[t$param != -1,"strat"] <- paste(t[t$param != -1,"strat"], t[t$param != -1, "param"], sep="-")
#t[!is.na(t$round), "alg"] <- paste(t[!is.na(t$round), "alg"], t[!is.na(t$round), "round"], sep='-')
#t <- t[t$round != "Precise",]
t$strat <- factor(t$strat,levels=c("Dyn","DChoice-10","DChoice-50","DCheaper","DMDA","Stat","SRand","SChoice","SCheaper", "chameleon"),labels=c("FirstDyn","ChoiceDyn-10","ChoiceDyn-50","EffectiveDyn","DMDA","Static","RandSteal","ChoiceSteal","EffectiveSteal", "Chameleon"))

  tsumm <- ddply(t, ~N+strat+alg+l+withcpu, summarize,
      	       timeM = mean(time), timemin = min(time), timemax=max(time),
      	       commM = mean(comm), commmin = min(comm), commmax=max(comm))

p <- ggplot(tsumm, aes(x=commM, y = timeM, xmin = commmin, xmax=commmax, ymin = timemin, ymax = timemax, color = strat, linetype=withcpu, shape=withcpu))
p <- plot_rect(p, "")
p <- p + theme(legend.box="vertical", legend.spacing.y=unit(0, "cm")) + geom_line()
p <- p + guides(linetype = guide_legend(title="",order=1), shape = guide_legend(title="",order=1), color = guide_legend(title="",nrow = 2,order=2))
ggsave("rect_cpubis.pdf", p, width = ratio * 7.1, height = ratio * height)
}



